from sklearn.mixture import GaussianMixture
import numpy as np
import pandas
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.patches import Ellipse
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
from typing import Union, Optional
import seaborn as sns
from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment

K_MAX = 20
N_REFERENCES = 5


class ExpectationMaximizationClustering(BaseExperiment):

    def __init__(self, data, result, name: str, log: bool = False):
        super().__init__(data, result, log)
        self._log = log
        self.name = name
        self.gaussian_search_df = None
        self.clustered_data = None
        self.gmm = None
        self._k_used = 3

    def generate_problem(self, seed=None):
        pass

    def plot(self, colors: Optional[str] = "Blues") -> None:
        plt.close()
        if isinstance(self.gaussian_search_df, pandas.DataFrame):
            plt.plot(self.gaussian_search_df['Cluster'], self.gaussian_search_df['SIL'], marker='o')
            plt.xlabel('Value of n_components')
            plt.ylabel('Silhouette Score')
            plt.title('Silhouette Method Gaussian Finding n_components')
            plt.savefig(f'./figures/{self.name}_n={self._k_used}_Clustering_Sil_Score.png')
            plt.close()
            plt.plot(self.gaussian_search_df['Cluster'], self.gaussian_search_df['DB'], marker='o')
            plt.xlabel('Value of n_components')
            plt.ylabel('DB Score')
            plt.title('DB Method Gaussian Finding n_components')
            plt.savefig(f'./figures/{self.name}_n={self._k_used}_Clustering_DB_Score.png')
            plt.close()
            plt.plot(self.gaussian_search_df['Cluster'], self.gaussian_search_df['CH'], marker='o')
            plt.xlabel('Value of n_components')
            plt.ylabel('CH Score')
            plt.title('CH Method Gaussian Finding n_components')
            plt.savefig(f'./figures/{self.name}_n={self._k_used}_Clustering_CH_Score.png')
            plt.close()

        if isinstance(self.clustered_data, pandas.DataFrame):
            g = sns.PairGrid(self.clustered_data, hue="label",
                             hue_order=[str(n) for n in range(self._k_used)] + [f"{str(n)} centroid" for n in
                                                                                range(self._k_used)],
                             # palette=["b", "r", "b", "r"],
                             hue_kws={"s": [20 for n in range(self._k_used)] + [500 for n in range(self._k_used)],
                                      "marker": ['o' for n in range(self._k_used)] + ['*' for n in range(self._k_used)]}
                             )
            g.map(plt.scatter, linewidth=1, edgecolor="w")
            g.add_legend()
            plt.title(f'{self.name}_n={self._k_used}_scatter_matrix_clustered')
            plt.savefig(f"./figures/CLUSTERED_{self.name}_n={self._k_used}_scatter_matrix.png")
        plt.close()



    def run_experiment(self, data, gaussians: Optional[int] = 3):
        self._k_used = gaussians
        # How to plot this well:
        # https://stackoverflow.com/questions/32008994/seaborn-scatterplot-matrix-adding-extra-points-with-custom-styles
        gmm = GaussianMixture(n_components=gaussians).fit(data)
        labels = GaussianMixture(n_components=gaussians).fit_predict(data)
        #print(f"the cluster centers were: {gmm.means_}")
        #print(f"the cluster columns were: {data.columns}")
        centroids = pd.DataFrame(gmm.means_, columns=data.columns)
        centroids["label"] = [f"{str(n)} mean" for n in range(gaussians)]
        data["label"] = labels.astype(str)
        self.clustered_data = pd.concat([data, centroids], ignore_index=True)
        self.gmm = gmm
        return labels.astype(str)

    # results: Union[np.ndarray, pandas.DataFrame],
    def find_best_gaussian_number(self, data: Union[np.ndarray, pandas.DataFrame], k_max: Optional[int] = None):
        # https://medium.com/analytics-vidhya/how-to-determine-the-optimal-k-for-k-means-708505d204eb
        sil = []
        sse = []
        ch = []
        db = []
        k_max = k_max if k_max is not None else K_MAX
        ks = range(2, k_max + 1)
        for k in ks:  # Must have 2 labels for siloutte scoring
            gmm = GaussianMixture(n_components=k).fit(data)
            labels = GaussianMixture(n_components=k).fit_predict(data)
            #print(f"Label shape {labels.shape} vs data shape {data.shape}")
            db_score = davies_bouldin_score(data, labels)
            ch_score = calinski_harabasz_score(data, labels)
            score = silhouette_score(data, labels, metric='euclidean')
            ch.append(ch_score)
            db.append(db_score)
            sil.append(score)

        self.gaussian_search_df = pd.DataFrame({'Cluster': ks, 'SIL': sil, "CH": ch, "DB": db})
