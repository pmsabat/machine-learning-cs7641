import numpy as np
import pandas
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
from sklearn.decomposition import NMF
from typing import Union, Optional
import seaborn as sns
from sklearn.utils._testing import ignore_warnings

from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment


class NNExperiment(BaseExperiment):

    def __init__(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            result: Union[np.ndarray, pd.DataFrame],
            name: str = "PCA",
            log: bool = False
    ):
        super().__init__(data, result, log)
        self.name = name
        self._reduced_data = data
        self.n = data.shape[1]

    def plot(self, colors: Optional[str] = "Blues") -> None:
        super().plot(colors=colors)

    def run_experiment(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            n_components: Optional[int] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:
        pass

    #https://gemfury.com/stream/python:scikit-learn/-/content/decomposition/tests/test_nmf.py
    # @staticmethod
    # @ignore_warnings
    # def test_non_negative_factorization_checking():
    #     A = np.ones((2, 2))
    #     # Test parameters checking is public function
    #     nnmf = nmf.non_negative_factorization
    #     msg = "Number of components must be positive; got (n_components='2')"
    #     assert_raise_message(ValueError, msg, nnmf, A, A, A, '2')
    #     msg = "Negative values in data passed to NMF (input H)"
    #     assert_raise_message(ValueError, msg, nnmf, A, A, -A, 2, 'custom')
    #     msg = "Negative values in data passed to NMF (input W)"
    #     assert_raise_message(ValueError, msg, nnmf, A, -A, A, 2, 'custom')
    #     msg = "Array passed to NMF (input H) is full of zeros"
    #     assert_raise_message(ValueError, msg, nnmf, A, A, 0 * A, 2, 'custom')
