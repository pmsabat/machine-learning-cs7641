from abc import ABC, abstractmethod
import logging as logger
from typing import Union, Optional

from timeit import default_timer as timer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.neural_network import MLPClassifier
import seaborn as sns
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


class BaseExperiment(ABC):

    def __init__(self, data: Union[np.ndarray, pd.DataFrame], result: Union[np.ndarray, pd.DataFrame], log: bool = False):
        self._nn_score_times = None
        self._nn_fit_scores = None
        self._nn_train_sizes = None
        self._nn_fit_times = None
        self._nn_train_scores = None
        self._predict_results = None
        self._nn_cm = None
        self._percentage = .75
        self._results = result
        self._data = data
        self._reduced_data = None
        self.name = "BaseExperiment"
        self._log = log
        self.k_search_df = None
        self._k_used = 3
        self._learner = None

    def log(self, msg: str):
        if self._log:
            logger.info(msg)

    def generate_problem(self, seed=None):
        self._learner = MLPClassifier(  # best settings from experiment 1
            activation="relu",
            solver="lbfgs",
            max_iter=200
        )

    def run_nn(self):
        self.generate_problem()
        x_data_train, x_data_test, y_data_train, y_data_test = train_test_split(
            self._reduced_data,
            self._results,
            test_size=1.0-self._percentage,
            random_state=0  # make it repeatable
        )
        #print(f"The y_data will be: {y_data_train}")

        nn_fit_time = timer()
        self._learner.fit(x_data_train, y_data_train.values.ravel())
        nn_fit_time_end = timer()
        nn_query_time = timer()
        self._predict_results = self._learner.predict(x_data_test)
        nn_query_time_end = timer()
        self._nn_cm = confusion_matrix(self._predict_results, y_data_test.values.ravel())
        self.generate_problem() # reset to do learning curve

        self._nn_train_sizes, self._nn_train_scores, self._nn_fit_scores, \
            self._nn_fit_times, self._nn_score_times = learning_curve(
                self._learner,
                self._reduced_data,
                self._results,
                cv=4,
                return_times=True,
            )

    @abstractmethod
    def plot(self, colors: Optional[str] = "Blues") -> None:
        self.plot_confusion_matrix(self._nn_cm, "nn", colors=colors)
        self.plot_learning_curve(
            "nn_learning_curve", self._nn_train_scores, self._nn_fit_scores, self._nn_fit_times, self._nn_score_times
        )
        plt.close()

    @abstractmethod
    def run_experiment(self, data) -> dict:
        pass

    def plot_3d_fit(self,  name, colors="Blues"):
        # From - https://www.stackvidhya.com/plot-confusion-matrix-in-python-and-why/
        try:
            plt.close()
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            ax.scatter(self._reduced_data[0], self._reduced_data[1], self._reduced_data[2])
            xAxisLine = ((min(self._reduced_data[0]), max(self._reduced_data[0])), (0, 0), (0, 0))
            ax.plot(xAxisLine[0], xAxisLine[1], xAxisLine[2], 'r')
            yAxisLine = ((0, 0), (min(self._reduced_data[1]), max(self._reduced_data[1])), (0, 0))
            ax.plot(yAxisLine[0], yAxisLine[1], yAxisLine[2], 'r')
            zAxisLine = ((0, 0), (0, 0), (min(self._reduced_data[2]), max(self._reduced_data[2])))
            ax.plot(zAxisLine[0], zAxisLine[1], zAxisLine[2], 'r')
            plt.title(f'3d_{self.name}_{name}_clustered')
            plt.savefig(f'./figures/3d_{self.name}_{name}_clustered.png')
            plt.close()
        except Exception as ex:
            print(f"Error plotting 3d clusters: {ex}")

    def plot_confusion_matrix(self, cm, name, colors="Blues"):
        # From - https://www.stackvidhya.com/plot-confusion-matrix-in-python-and-why/
        plt.close()
        plt.figure()
        ax = sns.heatmap(cm, annot=True, cmap=colors)

        ax.set_title(f'Confusion Matrix for {name} with labels');
        ax.set_xlabel('\nPredicted Values')
        ax.set_ylabel('Actual Values ');

        ## Ticket labels - List must be in alphabetical order
        ax.xaxis.set_ticklabels(['False', 'True'])
        ax.yaxis.set_ticklabels(['False', 'True'])
        plt.title(f'{self.name}_{name}_cm')
        ## Save the visualization of the Confusion Matrix.
        plt.savefig(f'./figures/{self.name}_{name}_cm.png')
        plt.close()

    def plot_learning_curve(
            self, title, train_scores, test_scores, fit_times, score_times,
            ylim=None, axes=None, train_sizes=np.linspace(0.1, 1.0, 5)
    ):
        """
            Generate 3 plots: the test and training learning curve, the training
            samples vs fit times curve, the fit times vs score curve.

            Parameters
            ----------

            title : str
                Title for the chart.

            axes : array-like of shape (3,), default=None
                Axes to use for plotting the curves.

            ylim : tuple of shape (2,), default=None
                Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

            train_sizes : array-like of shape (n_ticks,)
                Relative or absolute numbers of training examples that will be used to
                generate the learning curve. If the ``dtype`` is float, it is regarded
                as a fraction of the maximum size of the training set (that is
                determined by the selected validation method), i.e. it has to be within
                (0, 1]. Otherwise it is interpreted as absolute sizes of the training
                sets. Note that for classification the number of samples usually have
                to be big enough to contain at least one sample from each class.
                (default: np.linspace(0.1, 1.0, 5))
        """
        plt.close()
        plt.figure()
        if axes is None:
            _, axes = plt.subplots(1, 3, figsize=(20, 5))

        axes[0].set_title(title)
        if ylim is not None:
            axes[0].set_ylim(*ylim)
        axes[0].set_xlabel("Training examples")
        axes[0].set_ylabel("Score")

        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)
        fit_times_mean = np.mean(fit_times, axis=1)
        fit_times_std = np.std(fit_times, axis=1)

        # Plot learning curve
        axes[0].grid()
        axes[0].fill_between(
            train_sizes,
            train_scores_mean - train_scores_std,
            train_scores_mean + train_scores_std,
            alpha=0.1,
            color="r",
        )
        axes[0].fill_between(
            train_sizes,
            test_scores_mean - test_scores_std,
            test_scores_mean + test_scores_std,
            alpha=0.1,
            color="g",
        )
        axes[0].plot(
            train_sizes, train_scores_mean, "o-", color="r", label="Training score"
        )
        axes[0].plot(
            train_sizes, test_scores_mean, "o-", color="g", label="Cross-validation score"
        )
        axes[0].legend(loc="best")

        # Plot n_samples vs fit_times
        axes[1].grid()
        axes[1].plot(train_sizes, fit_times_mean, "o-")
        axes[1].fill_between(
            train_sizes,
            fit_times_mean - fit_times_std,
            fit_times_mean + fit_times_std,
            alpha=0.1,
        )
        axes[1].set_xlabel("Training examples")
        axes[1].set_ylabel("fit_times")
        axes[1].set_title("Scalability of the model")

        # Plot fit_time vs score
        fit_time_argsort = fit_times_mean.argsort()
        fit_time_sorted = fit_times_mean[fit_time_argsort]
        test_scores_mean_sorted = test_scores_mean[fit_time_argsort]
        test_scores_std_sorted = test_scores_std[fit_time_argsort]
        axes[2].grid()
        axes[2].plot(fit_time_sorted, test_scores_mean_sorted, "o-")
        axes[2].fill_between(
            fit_time_sorted,
            test_scores_mean_sorted - test_scores_std_sorted,
            test_scores_mean_sorted + test_scores_std_sorted,
            alpha=0.1,
        )
        axes[2].set_xlabel("fit_times")
        axes[2].set_ylabel("Score")
        axes[2].set_title("Performance of the model")

        plt.title(f'{self.name}_{title}')
        plt.savefig(f"./figures/{self.name}_{title}.png")
        plt.close()
