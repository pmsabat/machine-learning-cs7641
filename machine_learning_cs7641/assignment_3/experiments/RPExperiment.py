import numpy as np
import pandas as pd
from sklearn.random_projection import GaussianRandomProjection
from typing import Union, Optional
from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment

class RPExperiment(BaseExperiment):

    def __init__(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            result: Union[np.ndarray, pd.DataFrame],
            name: str = "RP",
            log: bool = False,
            n: Optional[int] = None
    ):
        super().__init__(data, result, log)
        self.name = name
        self.n = n
        self.k_search_df = None
        self.clustered_data = None
        self._k_used = 3

    def plot(self, colors: Optional[str] = "Blues") -> None:
        super().plot(colors=colors)

        if self.n is 3:
            super().plot_3d_fit(name="RP_Scatter", colors=colors)

    def run_experiment(
            self,
            n_components: Optional[Union[int, float]] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:

        reduced = self.reduce_data(self._data, n_components=n_components, random_seed=random_seed)
        self.run_nn()
        return reduced

    def reduce_data(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            n_components: Optional[int] = None,
            random_seed: Optional[Union[int, float]] = None
    ) -> Union[np.ndarray, pd.DataFrame]:
        rp = GaussianRandomProjection(random_state=random_seed, n_components=n_components)
        transformed = rp.fit_transform(data)
        self._reduced_data = pd.DataFrame(transformed, index=data.index)

        return self._reduced_data

    def find_variance_over_x_attempts(
            self,
            x: Optional[int] = 10000,
            n_components: Optional[int] = None
    ) -> None:
        best_accuracy = None
        worst_accuracy = None
        accuracy_vector = []
        data = self._data
        results = self._results
        for n in range(1, x+1):
            # seed = np.random.rand()
            self.reduce_data(data, n_components=n_components)
            self.run_nn()
            print(f"Confusion matrix was: {self._nn_cm}")
            tp = self._nn_cm[0][0]
            fn = self._nn_cm[0][1]
            fp = self._nn_cm[1][0]
            tn = self._nn_cm[1][1]
            accuracy = (tp + tn)/(tp + tn + fp + fn)
            best_accuracy = accuracy if best_accuracy is None or accuracy > best_accuracy else best_accuracy
            worst_accuracy = accuracy if worst_accuracy is None or accuracy < worst_accuracy else worst_accuracy
            accuracy_vector.append(accuracy)

        print(f"the best accuracy was: {best_accuracy}, the worst was: {worst_accuracy}, the average was: {np.mean(accuracy_vector)}, and variance was : {np.var(accuracy_vector)}")

