import numpy as np
import pandas
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
from sklearn.cluster import KMeans
from typing import Union, Optional
import seaborn as sns
from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment

K_MAX = 20
N_REFERENCES = 5


class KMeansClustering(BaseExperiment):

    def __init__(self, data, result, name: str, log: bool = False):
        super().__init__(data, result, log)
        self.name = name
        self.k_search_df = None
        self.clustered_data = None
        self._k_used = 3

    def generate_problem(self, seed=None):
        pass

    def plot(self, colors: Optional[str] = "Blues") -> None:
        plt.close()
        if isinstance(self.k_search_df, pandas.DataFrame):
            plt.plot(self.k_search_df['Cluster'], self.k_search_df['SSE'], marker='o')
            plt.xlabel('Value of K')
            plt.ylabel('Sum of Squared Error')
            plt.title('Elbow Method KMeans Finding K')
            plt.savefig(f'./figures/{self.name}_k={self._k_used}_Clustering_SSE.png')
            plt.close()
            plt.plot(self.k_search_df['Cluster'], self.k_search_df['SIL'], marker='o')
            plt.xlabel('Value of K')
            plt.ylabel('Silhouette Score')
            plt.title('Silhouette Method KMeans Finding K')
            plt.savefig(f'./figures/{self.name}_k={self._k_used}_Clustering_Sil_Score.png')
            plt.close()
            plt.plot(self.k_search_df['Cluster'], self.k_search_df['DB'], marker='o')
            plt.xlabel('Value of K')
            plt.ylabel('DB Score')
            plt.title('DB Method KMeans Finding K')
            plt.savefig(f'./figures/{self.name}_k={self._k_used}_Clustering_DB_Score.png')
            plt.close()
            plt.plot(self.k_search_df['Cluster'], self.k_search_df['CH'], marker='o')
            plt.xlabel('Value of K')
            plt.ylabel('CH Score')
            plt.title('CH Method KMeans Finding K')
            plt.savefig(f'./figures/{self.name}_k={self._k_used}_Clustering_CH_Score.png')
            plt.close()

        if isinstance(self.clustered_data, pandas.DataFrame):
            g = sns.PairGrid(self.clustered_data, hue="label",
                             hue_order=[str(n) for n in range(self._k_used)] + [f"{str(n)} centroid" for n in
                                                                                  range(self._k_used)],
                             # palette=["b", "r", "b", "r"],
                             hue_kws={"s": [20 for n in range(self._k_used)] + [500 for n in range(self._k_used)],
                                      "marker": ['o' for n in range(self._k_used)] + ['*' for n in range(self._k_used)]}
                             )
            g.map(plt.scatter, linewidth=1, edgecolor="w")
            g.add_legend()
            plt.title(f'{self.name}_k={self._k_used}_scatter_matrix_clustered')
            plt.savefig(f"./figures/CLUSTERED_{self.name}_k={self._k_used}_scatter_matrix.png")
        plt.close()

    def run_experiment(self, data, k: Optional[int] = 3) -> np.ndarray:
        self._k_used = k
        # How to plot this well:
        # https://stackoverflow.com/questions/32008994/seaborn-scatterplot-matrix-adding-extra-points-with-custom-styles
        km = KMeans(n_clusters=k).fit(data)
        #print(f"the cluster centers were: {km.cluster_centers_}")
        #print(f"the cluster columns were: {data.columns}")
        centroids = pd.DataFrame(km.cluster_centers_, columns=data.columns)
        centroids["label"] = [f"{str(n)} centroid" for n in range(k)]
        data["label"] = km.labels_.astype(str)
        self.clustered_data = pd.concat([data, centroids], ignore_index=True)

        return km.labels_.astype(str)


    # results: Union[np.ndarray, pandas.DataFrame],
    def find_best_k(self, data: Union[np.ndarray, pandas.DataFrame], k_max: Optional[int] = None):
        # https://medium.com/analytics-vidhya/how-to-determine-the-optimal-k-for-k-means-708505d204eb
        sil = []
        sse = []
        ch = []
        db = []
        k_max = k_max if k_max is not None else K_MAX
        ks = range(2, k_max+1)
        for k in ks: # Must have 2 labels for siloutte scoring
            kmeans = KMeans(n_clusters=k).fit(data)
            labels = kmeans.labels_
            #print(f"Label shape {labels.shape} vs data shape {data.shape}")
            y_pred = KMeans(n_clusters=k).fit_predict(data)
            db_score = davies_bouldin_score(data, y_pred)
            ch_score = calinski_harabasz_score(data, y_pred)
            score = silhouette_score(data, labels, metric='euclidean')
            ch.append(ch_score)
            db.append(db_score)
            sil.append(score)
            sse.append(kmeans.inertia_)

        self.k_search_df = pd.DataFrame({'Cluster': ks, 'SSE': sse, 'SIL': sil, "CH": ch, "DB": db})




