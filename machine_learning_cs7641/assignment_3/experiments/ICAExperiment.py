import numpy as np
import pandas as pd
from sklearn.decomposition import FastICA
from typing import Union, Optional
from scipy.stats import kurtosis

from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment

K_MAX = 20
N_REFERENCES = 5


class ICAExperiment(BaseExperiment):

    def __init__(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            result: Union[np.ndarray, pd.DataFrame],
            name: str = "ICA",
            log: bool = False,
            n: Optional[int] = None
    ):
        super().__init__(data, result, log)
        self.name = name
        self.n = n
        self._k_used = 3

    def plot(self, colors: Optional[str] = "Blues") -> None:
        super().plot(colors=colors)

        if self.n is 3:
            super().plot_3d_fit(name="ICA_Scatter", colors=colors)

    def run_experiment(
            self,
            n_components: Optional[int] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:

        reduced = self.reduce_data(self._data, n_components=n_components, random_seed=random_seed)
        self.run_nn()
        return reduced

    def reduce_data(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            n_components: Optional[int] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:
        ica = FastICA(random_state=random_seed, n_components=n_components)
        transformed = ica.fit_transform(data)
        self._reduced_data = pd.DataFrame(transformed, index=data.index)
        print(f"for ICA kurtosis is: {kurtosis(self._reduced_data)}")

        return self._reduced_data

