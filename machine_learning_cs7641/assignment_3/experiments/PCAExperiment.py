from typing import Union, Optional
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from machine_learning_cs7641.assignment_3.experiments.BaseExperiment import BaseExperiment


class PCAExperiment(BaseExperiment):

    def __init__(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            result: Union[np.ndarray, pd.DataFrame],
            name: str = "PCA",
            log: bool = False,
            n: Optional[int] = None
    ):
        super().__init__(data, result, log)
        self.name = name
        self.n = n

    def plot(self, colors: Optional[str] = "Blues") -> None:
        super().plot(colors=colors)

        # if self.n is 3:
        #     super().plot_3d_fit(name="PCA_Scatter", colors=colors)

    def run_experiment(
            self,
            n_components: Optional[int] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:
        reduced = self.reduce_data(self._data, n_components=n_components, random_seed=random_seed)
        self.n = reduced.shape[1]
        self.run_nn()
        return reduced

    def reduce_data(
            self,
            data: Union[np.ndarray, pd.DataFrame],
            n_components: Optional[int] = None,
            random_seed: Optional[int] = None
    ) -> Union[np.ndarray, pd.DataFrame]:
        pca = PCA(random_state=random_seed, n_components=n_components)
        transformed = pca.fit_transform(data)
        print(f"for PCA variance is: {pca.explained_variance_}")
        self._reduced_data = pd.DataFrame(transformed, index=data.index)



        return self._reduced_data

