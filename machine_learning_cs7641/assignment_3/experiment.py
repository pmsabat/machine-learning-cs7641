import pandas
import warnings
warnings.filterwarnings("ignore")
from machine_learning_cs7641.assignment_3.experiments.KMeansExperiment import KMeansClustering
from machine_learning_cs7641.assignment_3.experiments.ExpectationMaximizationExperiment import \
    ExpectationMaximizationClustering
from machine_learning_cs7641.assignment_3.experiments.SVDExperiment import SVDExperiment
from machine_learning_cs7641.assignment_3.experiments.PCAExperiment import PCAExperiment
from machine_learning_cs7641.assignment_3.experiments.ICAExperiment import ICAExperiment
from machine_learning_cs7641.assignment_3.experiments.RPExperiment import RPExperiment
from machine_learning_cs7641.assignment_3.experiments.NNExperiment import NNExperiment

SPAM_BEST_K = 2
SPAM_BEST_N = 3
SPAM_95_VARIANCE_N_COMPONENTS = 48
PHISHING_BEST_K = 3
PHISHING_BEST_N = 4
PHISHING_95_VARIANCE_N_COMPONENTS = 23

all_spam_df = pandas.read_csv("../data/spam.csv")
all_spam_df = all_spam_df.dropna()
spam_results_df = all_spam_df["class"].to_frame()
spam_data_df = all_spam_df.drop(columns=["class"])
spam_data_df = (spam_data_df - spam_data_df.mean()) / spam_data_df.std()
# sns.pairplot(all_spam_df, hue="class") # TAKES FOREVER, UNCOMMENT IF YOU WANT
# plt.savefig('./figures/spam_scatter_matrix.png')
print(f"Spam shape was: {spam_data_df.shape}")

print("Start spam RP variance experiment")
rp_var_exp = RPExperiment(spam_data_df, spam_results_df, name="Spam Variance RP experiment")
rp_var_exp.find_variance_over_x_attempts(x=10000, n_components=SPAM_95_VARIANCE_N_COMPONENTS)
print("End spam RP variance experiment")

all_phishing_df = pandas.read_csv("../data/phishing.csv")
all_phishing_df = all_phishing_df.dropna()
# all_phishing_df = (all_phishing_df - all_phishing_df.mean()) / all_phishing_df.std()
phishing_results_df = all_phishing_df["Result"].to_frame()
phishing_data_df = all_phishing_df.drop(columns=["Result"])
phishing_data_df = (phishing_data_df - phishing_data_df.mean()) / phishing_data_df.std()
print(f"Phishing Data shape was: {phishing_data_df.shape}")
# sns.pairplot(all_phishing_df, hue="Result") # TAKES FOREVER, UNCOMMENT IF YOU WANT
# plt.savefig('./figures/phishing_scatter_matrix.png')

print("Start phishing RP variance experiment")
rp_var_exp = RPExperiment(phishing_data_df, phishing_results_df, name="Phishing Variance RP experiment")
rp_var_exp.find_variance_over_x_attempts(x=10000, n_components=SPAM_95_VARIANCE_N_COMPONENTS)
print("End phishing RP variance experiment")

k_means_spam_exp = KMeansClustering(spam_data_df, spam_results_df, "K Means Spam")
k_means_spam_exp.find_best_k(spam_data_df)
k_means_spam_exp.run_experiment(spam_data_df, 2)
k_means_spam_exp.plot()


k_means_phishing_exp = KMeansClustering(phishing_data_df, phishing_results_df, "K Means Phishing")
k_means_phishing_exp.find_best_k(phishing_data_df, 20)
k_means_phishing_exp.run_experiment(phishing_data_df, 3)
k_means_phishing_exp.plot()

expectations_management_phishing_exp = ExpectationMaximizationClustering(phishing_data_df, phishing_results_df,
                                                                         "Expectation Management Phishing")
expectations_management_phishing_exp.find_best_gaussian_number(phishing_data_df)
expectations_management_phishing_exp.run_experiment(phishing_data_df, 3)
expectations_management_phishing_exp.plot()


expectations_management_spam_exp = ExpectationMaximizationClustering(spam_data_df, spam_results_df,
                                                                     "Expectation Management Spam")
expectations_management_spam_exp.find_best_gaussian_number(spam_data_df)
expectations_management_spam_exp.run_experiment(spam_data_df, 3)
expectations_management_spam_exp.plot()

stats = []

#Determine best K Spam
# pca_exp = PCAExperiment(spam_data_df, spam_results_df, name=f"PCA_SPAM_.95_variance")
# pca_spam_transformed = pca_exp.run_experiment(n_components=0.95)
# print(f"Best K for spam is .95 : {pca_spam_transformed.shape[1]} at .95 variance")
# pca_spam_transformed = pca_exp.run_experiment(n_components=0.90)
# print(f"Best K for spam is .90 : {pca_spam_transformed.shape[1]} at .90 variance")
# pca_spam_transformed = pca_exp.run_experiment(n_components=0.8)
# print(f"Best K for spam is .90 : {pca_spam_transformed.shape[1]} at .80 variance")
# pca_spam_transformed = pca_exp.run_experiment(n_components=0.7)
# print(f"Best K for spam is .90 : {pca_spam_transformed.shape[1]} at .70 variance")
# pca_spam_transformed = pca_exp.run_experiment(n_components=0.6)
# print(f"Best K for spam is .90 : {pca_spam_transformed.shape[1]} at .60 variance")
#
# # https://chrisalbon.com/code/machine_learning/feature_engineering/select_best_number_of_components_in_tsvd/
# svd_exp = SVDExperiment(spam_data_df, spam_results_df, name=f"SVD_SPAM_.95_variance")
# svd_spam_transformed = svd_exp.run_experiment(n_components=spam_data_df.shape[1]-1)
# svd_exp.best_n_components(goal_var=.95)
#
#
# pca_exp = PCAExperiment(phishing_data_df, phishing_results_df, name=f"PCA_PHISHING_K=.95_variance")
# pca_phishing_transformed = pca_exp.run_experiment(n_components=.95)
# print(f"Best K for spam is .95 : {pca_phishing_transformed.shape[1]} at .95 variance")
# pca_phishing_transformed = pca_exp.run_experiment(n_components=0.90)
# print(f"Best K for spam is .90 : {pca_phishing_transformed.shape[1]} at .90 variance")
# pca_phishing_transformed = pca_exp.run_experiment(n_components=0.8)
# print(f"Best K for spam is .90 : {pca_phishing_transformed.shape[1]} at .80 variance")
# pca_phishing_transformed = pca_exp.run_experiment(n_components=0.7)
# print(f"Best K for spam is .90 : {pca_phishing_transformed.shape[1]} at .70 variance")
# pca_phishing_transformed = pca_exp.run_experiment(n_components=0.6)
# print(f"Best K for spam is .90 : {pca_phishing_transformed.shape[1]} at .60 variance")
# pca_exp.plot()
# pca_phishing_transformed.to_csv(f"./data/pca_.95_variance_phishing_transformed.csv")  # raw data
# pca_exp.generate_problem()
# svd_exp = SVDExperiment(phishing_data_df, phishing_results_df, name=f"SVD_PHISHING_.95_variance")
# svd_phishing_transformed = svd_exp.run_experiment(n_components=phishing_data_df.shape[1]-1)
# svd_exp.best_n_components(goal_var=.95)
# print("Done SVD phishing")


# pca_exp.plot()
# pca_spam_transformed.to_csv(f"./data/pca_.95_variance_spam_transformed.csv")
# data = pca_exp._reduced_data
# pca_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
# pca_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
# k_means_spam_exp.name = "PCA K Means Spam .95"
# expectations_management_spam_exp.name = "PCA Expectation Management Spam .95"
# k_means_spam_exp.plot()
# expectations_management_spam_exp.plot()
# pca_exp.name = f"PCA_SPAM_.95_variance_with_clusters"
# pca_exp.run_nn()
# pca_exp.plot()

# pca_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
# pca_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
# k_means_phishing_exp.name = "PCA K Means Phishing .95"
# expectations_management_phishing_exp.name = "PCA Expectation Management Phishing .95"
# k_means_phishing_exp.plot()
# expectations_management_phishing_exp.plot()
# pca_exp.name = f"PCA_PHISHING_.95_variance_with_clusters"
# pca_exp.run_nn()
# pca_exp.plot()

########
#### Experiments to Perform Dimension Reduction without clustering ####
########
pca_exp = PCAExperiment(spam_data_df, spam_results_df, name=f"PCA_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}")
pca_spam_transformed = pca_exp.run_experiment(n_components=SPAM_95_VARIANCE_N_COMPONENTS)
pca_exp.plot()
pca_spam_transformed.to_csv(f"./data/pca_N={SPAM_95_VARIANCE_N_COMPONENTS}_spam_transformed.csv")
data = pca_exp._reduced_data
pca_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
pca_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
pca_exp.name = f"PCA_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
pca_exp.run_nn()
pca_exp.plot()

ica_exp = ICAExperiment(spam_data_df, spam_results_df, name=f"ICA_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}")
ica_spam_transformed = ica_exp.run_experiment(n_components=SPAM_95_VARIANCE_N_COMPONENTS)
ica_exp.plot()
ica_spam_transformed.to_csv(f"./data/ica_N={SPAM_95_VARIANCE_N_COMPONENTS}_spam_transformed.csv")
data = ica_exp._reduced_data
ica_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
ica_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
ica_exp.name = f"ICA_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
ica_exp.run_nn()
ica_exp.plot()

rp_exp = RPExperiment(spam_data_df, spam_results_df, name=f"RP_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}")
rp_spam_transformed = rp_exp.run_experiment(n_components=SPAM_95_VARIANCE_N_COMPONENTS)
rp_exp.plot()
rp_spam_transformed.to_csv(f"./data/rp_N={SPAM_95_VARIANCE_N_COMPONENTS}_spam_transformed.csv")
data = rp_exp._reduced_data
rp_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
rp_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
rp_exp.name = f"RP_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
rp_exp.run_nn()
rp_exp.plot()

svd_exp = SVDExperiment(spam_data_df, spam_results_df, name=f"SVD_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}")
svd_spam_transformed = svd_exp.run_experiment(n_components=SPAM_95_VARIANCE_N_COMPONENTS)
svd_exp.plot()
svd_spam_transformed.to_csv(f"./data/svd_N={SPAM_95_VARIANCE_N_COMPONENTS}_spam_transformed.csv")
data = svd_exp._reduced_data
svd_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
svd_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
svd_exp.name = f"SVD_SPAM_N={SPAM_95_VARIANCE_N_COMPONENTS}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
svd_exp.run_nn()
svd_exp.plot()

pca_exp = PCAExperiment(phishing_data_df, phishing_results_df, name=f"PCA_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}")
pca_phishing_transformed = pca_exp.run_experiment(n_components=PHISHING_95_VARIANCE_N_COMPONENTS)
pca_exp.plot()
pca_phishing_transformed.to_csv(f"./data/pca_N={PHISHING_95_VARIANCE_N_COMPONENTS}_phishing_transformed.csv")  # raw data
pca_exp.generate_problem()
data = pca_exp._reduced_data
pca_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
pca_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
pca_exp.name = f"PCA_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
pca_exp.run_nn()
pca_exp.plot()

ica_exp = ICAExperiment(phishing_data_df, phishing_results_df, name=f"ICA_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}")
ica_phishing_transformed = ica_exp.run_experiment(n_components=PHISHING_95_VARIANCE_N_COMPONENTS)
ica_exp.plot()
ica_phishing_transformed.to_csv(f"./data/ica_N={PHISHING_95_VARIANCE_N_COMPONENTS}_phishing_transformed.csv")
data = ica_exp._reduced_data
ica_exp.generate_problem()
ica_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
ica_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
ica_exp.name = f"ICA_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
ica_exp.run_nn()
ica_exp.plot()

rp_exp = RPExperiment(phishing_data_df, phishing_results_df, name=f"RP_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}")
rp_phishing_transformed = rp_exp.run_experiment(n_components=PHISHING_95_VARIANCE_N_COMPONENTS)
rp_exp.plot()
rp_phishing_transformed.to_csv(f"./data/rp_N={PHISHING_95_VARIANCE_N_COMPONENTS}_phishing_transformed.csv")
data = rp_exp._reduced_data
rp_exp.generate_problem()
rp_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
rp_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
rp_exp.name = f"RP_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
rp_exp.run_nn()
rp_exp.plot()

svd_exp = SVDExperiment(phishing_data_df, phishing_results_df, name=f"SVD_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}")
svd_phishing_transformed = svd_exp.run_experiment(n_components=PHISHING_95_VARIANCE_N_COMPONENTS)
svd_exp.plot()
svd_phishing_transformed.to_csv(f"./data/svd_N={PHISHING_95_VARIANCE_N_COMPONENTS}_phishing_transformed.csv")
data = svd_exp._reduced_data
svd_exp.generate_problem()
svd_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
svd_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
svd_exp.name = f"SVD_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
svd_exp.run_nn()
svd_exp.plot()

# #### Experiments to Perform Dimension Reduction ####
#
# print(f"""The spam transformed data was:
#             pca: {pca_spam_transformed} with shape: {pca_spam_transformed.shape},
#             ica: {ica_spam_transformed} with shape: {ica_spam_transformed.shape},
#             rp: {rp_spam_transformed} with shape: {rp_spam_transformed.shape},
#     compared to original {spam_data_df.shape}""")

# GET NICE GRAPHS FOR CLUSTERING:
pca_exp = PCAExperiment(spam_data_df, spam_results_df, name=f"PCA_SPAM_N={3}")  # get 3d data
pca_spam_transformed = pca_exp.run_experiment(n_components=3)
pca_exp.plot()
pca_spam_transformed.to_csv(f"./data/pca_N={3}_spam_transformed.csv")
data = pca_exp._reduced_data
pca_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
pca_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
k_means_spam_exp.name = "PCA K Means Spam"
expectations_management_spam_exp.name = "PCA Expectation Management Spam"
k_means_spam_exp.plot()
expectations_management_spam_exp.plot()
pca_exp.name = f"PCA_SPAM_N={3}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
pca_exp.run_nn()
pca_exp.plot()

ica_exp = ICAExperiment(spam_data_df, spam_results_df, name=f"ICA_SPAM_N={3}")
ica_spam_transformed = ica_exp.run_experiment(n_components=3)
ica_exp.plot()
ica_spam_transformed.to_csv(f"./data/ica_N={3}_spam_transformed.csv")
data = ica_exp._reduced_data
ica_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
ica_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
k_means_spam_exp.name = "ICA K Means Spam"
expectations_management_spam_exp.name = "ICA Expectation Management Spam"
k_means_spam_exp.plot()
expectations_management_spam_exp.plot()
ica_exp.name = f"ICA_SPAM_N={3}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
ica_exp.run_nn()
ica_exp.plot()

rp_exp = RPExperiment(spam_data_df, spam_results_df, name=f"RP_SPAM_N={3}")
rp_spam_transformed = rp_exp.run_experiment(n_components=3)
rp_exp.plot()
rp_spam_transformed.to_csv(f"./data/rp_N={3}_spam_transformed.csv")
data = rp_exp._reduced_data
rp_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
rp_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
k_means_spam_exp.name = "RP K Means Spam"
expectations_management_spam_exp.name = "RP Expectation Management Spam"
k_means_spam_exp.plot()
expectations_management_spam_exp.plot()
rp_exp.name = f"RP_SPAM_N={3}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
rp_exp.run_nn()
rp_exp.plot()

svd_exp = SVDExperiment(spam_data_df, spam_results_df, name=f"SVD_SPAM_N={3}")
svd_spam_transformed = svd_exp.run_experiment(n_components=3)
svd_exp.plot()
svd_spam_transformed.to_csv(f"./data/svd_N={3}_spam_transformed.csv")
data = svd_exp._reduced_data
svd_exp._reduced_data["k_labels"] = k_means_spam_exp.run_experiment(data, SPAM_BEST_K)
svd_exp._reduced_data["gm_labels"] = expectations_management_spam_exp.run_experiment(data, SPAM_BEST_N)
k_means_spam_exp.name = "SVD K Means Spam"
expectations_management_spam_exp.name = "SVD Expectation Management Spam"
k_means_spam_exp.plot()
expectations_management_spam_exp.plot()
svd_exp.name = f"SVD_SPAM_N={3}_with_K={SPAM_BEST_K}&GM={SPAM_BEST_N}_clusters"
svd_exp.run_nn()
svd_exp.plot()

pca_exp = PCAExperiment(phishing_data_df, phishing_results_df, name=f"PCA_PHISHING_N={3}")
pca_phishing_transformed = pca_exp.run_experiment(n_components=3)
pca_exp.plot()
pca_phishing_transformed.to_csv(f"./data/pca_N={3}_phishing_transformed.csv")  # raw data
pca_exp.generate_problem()
data = pca_exp._reduced_data
pca_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
pca_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
k_means_phishing_exp.name = "PCA K Means Phishing"
expectations_management_phishing_exp.name = "PCA Expectation Management Phishing"
k_means_phishing_exp.plot()
expectations_management_phishing_exp.plot()
pca_exp.name = f"PCA_PHISHING_N={3}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
pca_exp.run_nn()
pca_exp.plot()

ica_exp = ICAExperiment(phishing_data_df, phishing_results_df, name=f"ICA_PHISHING_N={3}")
ica_phishing_transformed = ica_exp.run_experiment(n_components=3)
ica_exp.plot()
ica_phishing_transformed.to_csv(f"./data/ica_N={3}_phishing_transformed.csv")
data = ica_exp._reduced_data
ica_exp.generate_problem()
ica_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
ica_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
k_means_phishing_exp.name = "ICA K Means Phishing"
expectations_management_phishing_exp.name = "ICA Expectation Management Phishing"
k_means_phishing_exp.plot()
expectations_management_phishing_exp.plot()
ica_exp.name = f"ICA_PHISHING_N={3}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
ica_exp.run_nn()
ica_exp.plot()

rp_exp = RPExperiment(phishing_data_df, phishing_results_df, name=f"RP_PHISHING_N={3}")
rp_phishing_transformed = rp_exp.run_experiment(n_components=3)
rp_exp.plot()
rp_phishing_transformed.to_csv(f"./data/rp_N={3}_phishing_transformed.csv")
data = rp_exp._reduced_data
rp_exp.generate_problem()
rp_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
rp_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
k_means_phishing_exp.name = "RP K Means Phishing"
expectations_management_phishing_exp.name = "RP Expectation Management Phishing"
k_means_phishing_exp.plot()
expectations_management_phishing_exp.plot()
rp_exp.name = f"RP_PHISHING_N={3}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
rp_exp.run_nn()
rp_exp.plot()

svd_exp = SVDExperiment(phishing_data_df, phishing_results_df, name=f"SVD_PHISHING_N={PHISHING_95_VARIANCE_N_COMPONENTS}")
svd_phishing_transformed = svd_exp.run_experiment(n_components=PHISHING_95_VARIANCE_N_COMPONENTS)
svd_exp.plot()
svd_phishing_transformed.to_csv(f"./data/svd_N={3}_phishing_transformed.csv")
data = svd_exp._reduced_data
svd_exp.generate_problem()
svd_exp._reduced_data["k_labels"] = k_means_phishing_exp.run_experiment(data, PHISHING_BEST_K)
svd_exp._reduced_data["gm_labels"] = expectations_management_phishing_exp.run_experiment(data, PHISHING_BEST_N)
k_means_phishing_exp.name = "SVD K Means Phishing"
expectations_management_phishing_exp.name = "SVD Expectation Management Phishing"
k_means_phishing_exp.plot()
expectations_management_phishing_exp.plot()
svd_exp.name = f"SVD_PHISHING_N={3}_with_K={PHISHING_BEST_K}&GM={PHISHING_BEST_N}_clusters"
svd_exp.run_nn()
svd_exp.plot()

# Cheat our base learner into the nice class to get figures for it
phishing_nn_exp = NNExperiment(phishing_data_df, phishing_results_df, name="Original NN Phishing")
phishing_nn_exp.run_nn()
phishing_nn_exp.plot()

spam_nn_exp = NNExperiment(spam_data_df, spam_results_df, name="Original NN spam")
spam_nn_exp.run_nn()
spam_nn_exp.plot()

