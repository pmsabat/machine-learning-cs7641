from .BaseLearner import BaseLearner
import pandas as pd
import pathlib
from pandas import DataFrame
from sklearn.model_selection import train_test_split
from sklearn.ensemble import AdaBoostClassifier
import stackprinter
from typing import Union, List


class BoostedLearner(BaseLearner):

    def __init__(self, base_estimator: object = None, n_estimators: int = 50, learning_rate: float = 1.0,
                 algorithm: str = 'SAMME.R', random_state: int = None, log: bool = False) -> None:

        super().__init__(log)
        self.learner = AdaBoostClassifier(
            base_estimator=base_estimator,
            n_estimators=n_estimators,
            learning_rate=learning_rate,
            algorithm=algorithm,
            random_state=random_state
        )
        self.__name__ = "BoostedLearner"

    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass

if __name__ == "__main__":
    # This test setup heavily inspired by:
    # https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python
    from sklearn import datasets
    cancer_data_raw: dict = datasets.load_breast_cancer()
    # print("Features: ", cancer_data_raw.feature_names)
    cancer_data: DataFrame = pd.DataFrame(data=cancer_data_raw.data, columns=cancer_data_raw.feature_names)
    # cancer labels (0:malignant, 1:benign)
    cancer_result: DataFrame = pd.DataFrame(data=cancer_data_raw.target, columns=["malign/benign"])

    print(cancer_result.head(5))
    # 70% training and 30% test
    cancer_data_train, cancer_data_test, cancer_result_train, cancer_result_test = train_test_split(cancer_data,
                                                                                                    cancer_result,
                                                                                                    test_size=0.2,
                                                                                                    random_state=109)

    # df_cancer_predictions: DataFrame = pd.DataFame(data=cancer_predictions, columns=["malign/benign"])
    df_cancer_result_test: DataFrame = pd.DataFrame(data=cancer_result_test, columns=["malign/benign"])
    boosted_learner = BoostedLearner()
    boosted_learner.fit(cancer_data_train, cancer_result_train)
    boosted_accuracy = boosted_learner.accuracy(cancer_data_test, df_cancer_result_test)

    print(f"DT Accuracy: {boosted_accuracy}")