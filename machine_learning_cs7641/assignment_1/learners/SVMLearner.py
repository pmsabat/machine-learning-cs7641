from .BaseLearner import BaseLearner
import pandas as pd
import pathlib
from pandas import DataFrame
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from typing import Union, Callable
import stackprinter

stackprinter.set_excepthook(style='darkbg2')


class SVMLearner(BaseLearner):

    def __init__(self, c: float = 1, kernel: str = 'linear', degree: int = 3, gamma: Union[str, float] = 'scale',
                 coefficient0: float = 0.0, shrinking: bool = True, probability: bool = False, tolerance: float = 0.001,
                 cache_size: float = 200, class_weight: Union[dict, str] = None, max_iter: int = 1,
                 decision_function_shape: str = 'ovr', log: bool = False):
        super().__init__(log)
        self.learner = SVC(
            C=c,
            kernel=kernel,
            degree=degree,
            gamma=gamma,
            coef0=coefficient0,
            shrinking=shrinking,
            probability=probability,
            tol=tolerance,
            cache_size=cache_size,
            class_weight=class_weight,
            max_iter=max_iter,
            decision_function_shape=decision_function_shape,
            verbose=log
        )
        self.__name__ = "SVMLearner"

    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass


if __name__ == "__main__":
    # This test setup heavily inspired by:
    # https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python
    from sklearn import datasets
    cancer_data_raw: dict = datasets.load_breast_cancer()
    # print("Features: ", cancer_data_raw.feature_names)
    cancer_data: DataFrame = pd.DataFrame(data=cancer_data_raw.data, columns=cancer_data_raw.feature_names)
    # cancer labels (0:malignant, 1:benign)
    cancer_result: DataFrame = pd.DataFrame(data=cancer_data_raw.target, columns=["malign/benign"])
    # 70% training and 30% test
    cancer_data_train, cancer_data_test, cancer_result_train, cancer_result_test = train_test_split(cancer_data,
                                                                                                    cancer_result,
                                                                                                    test_size=0.2,
                                                                                                    random_state=109)

    # df_cancer_predictions: DataFrame = pd.DataFame(data=cancer_predictions, columns=["malign/benign"])
    df_cancer_result_test: DataFrame = pd.DataFrame(data=cancer_result_test, columns=["malign/benign"])
    linear_svm = SVMLearner()
    linear_svm.fit(cancer_data_train, cancer_result_train)
    linear_accuracy = linear_svm.accuracy(cancer_data_test, df_cancer_result_test)

    sigmoid_svm = SVMLearner(kernel="sigmoid")
    sigmoid_svm.fit(cancer_data_train, cancer_result_train)
    sigmoid_accuracy = sigmoid_svm.accuracy(cancer_data_test, df_cancer_result_test)

    rbf_svm = SVMLearner(kernel="rbf")
    rbf_svm.fit(cancer_data_train, cancer_result_train)
    rbf_accuracy = sigmoid_svm.accuracy(cancer_data_test, df_cancer_result_test)

    print(f"SVM Linear Accuracy: {linear_accuracy}")
    print(f"SVM Sigmoid Accuracy: {sigmoid_accuracy}")
    print(f"SVM Rbf Accuracy: {rbf_accuracy}")
