from .BaseLearner import BaseLearner
import numpy as np
import pandas as pd
import pathlib
from pandas import DataFrame
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
import stackprinter
from typing import Union, List


class DTLearner(BaseLearner):

    def __init__(self, criterion: str = 'gini', splitter: str = 'best', max_depth: int = None,
                 min_samples_split: Union[int, float] = 2, min_samples_leaf : Union[int, float] = 1,
                 min_weight_fraction_leaf: float = 0.0, max_features: Union[int, float, str] = None,
                 random_state: int = None, max_leaf_nodes: int = None, min_impurity_decrease: float = 0.0,
                 class_weight: Union[dict, List[dict], str] = None, ccp_alpha: float = 0.0, log: bool = False) -> None:

        self._x_train = None
        self._y_train = None
        self._criterion = criterion
        self._splitter = splitter
        self._max_depth = max_depth
        self._min_samples_split = min_samples_split
        self._min_samples_leaf = min_samples_leaf
        self._min_weight_fraction_leaf = min_weight_fraction_leaf
        self._max_features = max_features
        self._random_state = random_state
        self._max_leaf_nodes = max_leaf_nodes
        self._min_impurity_decrease = min_impurity_decrease
        self._class_weight = class_weight
        self._original_ccp_alpha = ccp_alpha
        self._ccp_alpha = ccp_alpha
        self.best_accuracy = 0

        super().__init__(log)
        self.learner = DecisionTreeClassifier(
            criterion=criterion,
            splitter=splitter,
            max_depth=max_depth, # affects pruning
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            random_state=random_state,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            class_weight=class_weight,
            ccp_alpha=ccp_alpha # affects pruning
        )
        self.__name__ = "DTLearner"

    # pruning implementation is heavily inspired from -
    # https://medium.com/analytics-vidhya/post-pruning-and-pre-pruning-in-decision-tree-561f3df73e65
    # MUST call fit first!
    def prune(self):
        path = self.learner.cost_complexity_pruning_path(self._x_train, self._y_train)
        ccp_alphas, impurities = path.ccp_alphas, path.impurities
        # Set up impartial training data - split to get a smaller "test" data set
        x_train, x_test, y_train, y_test = train_test_split(self._x_train, self._y_train, test_size=0.2)
        for ccp_alpha in ccp_alphas:
            tmp_dt_learner = DecisionTreeClassifier(
                criterion=self._criterion,
                splitter=self._splitter,
                max_depth=self._max_depth, # affects pruning
                min_samples_split=self._min_samples_split,
                min_samples_leaf=self._min_samples_leaf,
                min_weight_fraction_leaf=self._min_weight_fraction_leaf,
                max_features=self._max_features,
                random_state=self._random_state,
                max_leaf_nodes=self._max_leaf_nodes,
                min_impurity_decrease=self._min_impurity_decrease,
                class_weight=self._class_weight,
                ccp_alpha=ccp_alpha # affects pruning
            )

            tmp_dt_learner.fit(x_train, y_train)
            predictions = self.predict(x_test)
            accuracy = metrics.accuracy_score(y_test, predictions)
            if not self.best_accuracy or accuracy > self.best_accuracy:
                self.best_accuracy = accuracy  # if this is better, save that
                self._ccp_alpha = ccp_alpha # update best alpha

        # Done looping, lets use our winner
        self.learner = DecisionTreeClassifier(
            criterion=self._criterion,
            splitter=self._splitter,
            max_depth=self._max_depth, # affects pruning
            min_samples_split=self._min_samples_split,
            min_samples_leaf=self._min_samples_leaf,
            min_weight_fraction_leaf=self._min_weight_fraction_leaf,
            max_features=self._max_features,
            random_state=self._random_state,
            max_leaf_nodes=self._max_leaf_nodes,
            min_impurity_decrease=self._min_impurity_decrease,
            class_weight=self._class_weight,
            ccp_alpha=self._ccp_alpha # affects pruning
        )

        return self.learner.fit(self._x_train, self._y_train)

    def fit(self, data: [np.ndarray, pd.DataFrame], classes: [np.ndarray, pd.DataFrame] = None):
        self._x_train = data
        self._y_train = classes
        return self.learner.fit(data, classes)

    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass


if __name__ == "__main__":
    # This test setup heavily inspired by:
    # https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python
    from sklearn import datasets
    cancer_data_raw: dict = datasets.load_breast_cancer()
    # print("Features: ", cancer_data_raw.feature_names)
    cancer_data: DataFrame = pd.DataFrame(data=cancer_data_raw.data, columns=cancer_data_raw.feature_names)
    # cancer labels (0:malignant, 1:benign)
    cancer_result: DataFrame = pd.DataFrame(data=cancer_data_raw.target, columns=["malign/benign"])

    print(cancer_result.head(5))
    # 70% training and 30% test
    cancer_data_train, cancer_data_test, cancer_result_train, cancer_result_test = train_test_split(cancer_data,
                                                                                                    cancer_result,
                                                                                                    test_size=0.2,
                                                                                                    random_state=109)

    # df_cancer_predictions: DataFrame = pd.DataFame(data=cancer_predictions, columns=["malign/benign"])
    df_cancer_result_test: DataFrame = pd.DataFrame(data=cancer_result_test, columns=["malign/benign"])
    dt_learner = DTLearner()
    dt_learner.fit(cancer_data_train, cancer_result_train)
    dt_accuracy = dt_learner.accuracy(cancer_data_test, df_cancer_result_test)
    print(f"Pre-pruning DT Accuracy: {dt_accuracy}")
    dt_learner.prune()
    pruned_dt_accuracy = dt_learner.accuracy(cancer_data_test, df_cancer_result_test)
    print(f"Post-pruning DT Accuracy: {pruned_dt_accuracy}")

