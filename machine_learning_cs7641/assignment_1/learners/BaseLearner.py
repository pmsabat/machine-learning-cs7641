import logging as logger
import pathlib
import numpy as np
import pandas as pd
import stackprinter
from abc import ABC, abstractmethod
from sklearn import metrics
from sklearn.base import BaseEstimator, ClassifierMixin


# https://sklearn-template.readthedocs.io/en/latest/user_guide.html#classifier
class BaseLearner(ABC, BaseEstimator, ClassifierMixin):
    """
    The abstract class for others to inherit from
    """
    def __init__(self, log: bool):
        self._log: bool = log
        self.learner = None

    def log(self, msg: str):
        if self._log:
            logger.info(msg)

    def fit(self, data: [np.ndarray, pd.DataFrame], classes: [np.ndarray, pd.DataFrame] = None) -> object:
        return self.learner.fit(data, classes)

    def predict(self, data: [np.ndarray, pd.DataFrame]) -> any:
        return self.learner.predict(data)

    def accuracy(self, test_data, result_data, normalize=True, sample_weight: any = None) -> float:
        predictions = self.predict(test_data)
        return metrics.accuracy_score(result_data, predictions, normalize=normalize, sample_weight=sample_weight)

    @abstractmethod
    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass
