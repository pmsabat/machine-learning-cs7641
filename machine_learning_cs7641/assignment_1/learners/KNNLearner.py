from .BaseLearner import BaseLearner
import pandas as pd
import pathlib
from pandas import DataFrame
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
import stackprinter
from typing import Union, Callable

stackprinter.set_excepthook(style='darkbg2')


class KNNLearner(BaseLearner):

    def __init__(self, n_neighbors: int = 5, weights: Union[str, Callable] = 'uniform', algorithm: str = 'auto',
                 leaf_size: int = 30, p: int = 2, metric: str = 'minkowski',
                 metric_params: dict = None, n_jobs: int = None, log: bool = False) -> None:
        super().__init__(log)
        self.learner = KNeighborsClassifier(
            n_neighbors=n_neighbors,
            weights=weights,
            algorithm=algorithm,
            leaf_size=leaf_size,
            p=p,
            metric=metric,
            metric_params=metric_params,
            n_jobs=n_jobs
        )
        self.__name__ = "KNNLearner"

    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass


if __name__ == "__main__":
    # This test setup heavily inspired by:
    # https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python
    from sklearn import datasets
    cancer_data_raw: dict = datasets.load_breast_cancer()
    # print("Features: ", cancer_data_raw.feature_names)
    cancer_data: DataFrame = pd.DataFrame(data=cancer_data_raw.data, columns=cancer_data_raw.feature_names)
    # cancer labels (0:malignant, 1:benign)
    cancer_result: DataFrame = pd.DataFrame(data=cancer_data_raw.target, columns=["malign/benign"])
    # 70% training and 30% test
    print(cancer_result.head(5))
    cancer_data_train, cancer_data_test, cancer_result_train, cancer_result_test = train_test_split(cancer_data,
                                                                                                    cancer_result,
                                                                                                    test_size=0.2,
                                                                                                    random_state=109)

    # df_cancer_predictions: DataFrame = pd.DataFame(data=cancer_predictions, columns=["malign/benign"])
    df_cancer_result_test: DataFrame = pd.DataFrame(data=cancer_result_test, columns=["malign/benign"])
    k_nearest_neighbors_model = KNNLearner()
    k_nearest_neighbors_model.fit(cancer_data_train, cancer_result_train)
    k_nearest_neighbors_accuracy = k_nearest_neighbors_model.accuracy(cancer_data_test, df_cancer_result_test)

    print(f"KNN Accuracy: {k_nearest_neighbors_accuracy}")
