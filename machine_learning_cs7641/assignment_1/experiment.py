import pandas
from machine_learning_cs7641.assignment_1.experiments.KNNExperiment import KNNExperiment
from machine_learning_cs7641.assignment_1.experiments.NeuralNetworkExperiment import NeuralNetworkExperiment
from machine_learning_cs7641.assignment_1.experiments.BoostExperiment import BoostExperiment
from machine_learning_cs7641.assignment_1.experiments.DTExperiments import DTExperiment
from machine_learning_cs7641.assignment_1.experiments.SVMExperiment import SVMExperiment
import seaborn as sns  # https://seaborn.pydata.org/tutorial/relational.html
sns.set_theme(style="darkgrid")


def run() -> None:
    # spam data
    all_spam_df = pandas.read_csv("../data/spam.csv")
    spam_results_df = all_spam_df["class"].to_frame()
    spam_data_df = all_spam_df.drop(columns=["class"])
    spam_samples = spam_data_df.size

    all_phishing_df = pandas.read_csv("../data/phishing.csv")
    phishing_results_df = all_phishing_df["Result"].to_frame()
    phishing_data_df = all_phishing_df.drop(columns=["Result"])
    phishing_samples = phishing_data_df.size

    nn_experiment_spam = NeuralNetworkExperiment(spam_data_df, spam_results_df)
    nn_experiment_phishing = NeuralNetworkExperiment(phishing_data_df, phishing_results_df)
    nn_spam_results = nn_experiment_spam.run(training_set_name="SPAM")
    nn_phishing_results = nn_experiment_phishing.run(training_set_name="PHISHING")
    nn_results = nn_spam_results + nn_phishing_results

    svm_experiment_spam = SVMExperiment(spam_data_df, spam_results_df)
    svm_experiment_phishing = SVMExperiment(phishing_data_df, phishing_results_df)
    svm_spam_results = svm_experiment_spam.run(training_set_name="SPAM")
    svm_phishing_results = svm_experiment_phishing.run(training_set_name="PHISHING")
    svm_results = svm_spam_results + svm_phishing_results

    knn_experiment_spam = KNNExperiment(spam_data_df, spam_results_df)
    knn_experiment_phishing = KNNExperiment(phishing_data_df, phishing_results_df)
    knn_spam_results = knn_experiment_spam.run(training_set_name="SPAM")
    knn_phishing_results = knn_experiment_phishing.run(training_set_name="PHISHING")
    knn_results = knn_spam_results + knn_phishing_results

    boost_experiment_spam = BoostExperiment(spam_data_df, spam_results_df)
    boost_experiment_phishing = BoostExperiment(phishing_data_df, phishing_results_df)
    boosted_spam_results = boost_experiment_spam.run(training_set_name="SPAM")
    boosted_phishing_results = boost_experiment_phishing.run(training_set_name="PHISHING")
    boosted_results = boosted_spam_results + boosted_phishing_results

    dt_experiment_spam = DTExperiment(spam_data_df, spam_results_df)
    dt_experiment_phishing = DTExperiment(phishing_data_df, phishing_results_df)
    dt_spam_results = dt_experiment_spam.run(training_set_name="SPAM")
    dt_phishing_results = dt_experiment_phishing.run(training_set_name="PHISHING")
    dt_results = dt_spam_results + dt_phishing_results

    results = []
    results.extend(svm_results)
    results.extend(boosted_results)
    results.extend(nn_results)
    results.extend(knn_results)
    results.extend(dt_results)
    results_df = pandas.DataFrame.from_records(results)
    results_df.to_csv("./experiment_results.csv")
    learning_curve_plt = sns.relplot(x="samples", y="fit_time", hue="learner_name", data=results_df)
    learning_curve_plt.savefig("./learning-curve-plot.png")

    print(f"Lets see it: \n {results_df}")


if __name__ == "__main__":
    run()
