from abc import ABC, abstractmethod
import logging as logger
from machine_learning_cs7641.assignment_1.learners.BaseLearner import BaseLearner
import pandas
from timeit import default_timer as timer
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, accuracy_score
from machine_learning_cs7641.assignment_1.learners.BoostedLearner import BoostedLearner
import matplotlib.pyplot as plt


class BaseExperiment(ABC):

    def __init__(self, x_data: np.ndarray, y_data: np.ndarray, log: bool = False):
        self._log: bool = log
        self._data = x_data
        self._results = y_data
        self._learner = None
        self.experiment_results = []

    def log(self, msg: str):
        if self._log:
            logger.info(msg)

    @abstractmethod
    def initialize_learner(self):
        pass

    @abstractmethod
    def run(self, training_set_name: str = None) -> list:
        pass

    def experiment(self, run_name: str, percentage: float, training_set_name: str = None, prune: bool = False) -> dict:
        if self._learner is None:
            self.initialize_learner()  # use reasonable defaults if learner is not initiated

        result = {
            "run_name": run_name,
            "learner_name": self._learner.__name__,
            "samples": (percentage * self._data.size),
            "dataset": "UNKNOWN" if not training_set_name else training_set_name
        }

        x_data_train, x_data_test, y_data_train, y_data_test = train_test_split(
            self._data,
            self._results,
            test_size=1.0-percentage,
            random_state=0  # make it repeatable
        )

        t0 = timer()
        self._learner.fit(x_data_train, y_data_train.values.ravel())
        if hasattr(self._learner, "prune") and prune is True:
            print("PRUNING")
            self._learner.prune()
        t1 = timer()
        result["fit_time"] = t1 - t0

        t2 = timer()
        predict_results = self._learner.predict(x_data_test)
        t3 = timer()
        result["query_time"] = t3 - t2

        #  https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal
        result["accuracy"] = accuracy_score(predict_results, y_data_test)
        cm = confusion_matrix(predict_results, y_data_test)
        result["false_positives"] = cm[0][1]
        result["false_negatives"] = cm[1][0]
        result["true_positives"] = cm[1][1]
        result["true_negatives"] = cm[0][0]
        result["tp_rate"] = result["true_positives"] / (result["true_positives"] + result["false_negatives"])
        result["tn_rate"] = result["true_negatives"] / (result["true_negatives"] + result["false_positives"])
        result["positive_predictive_value"] = result["true_positives"] / (
                result["true_positives"] + result["false_positives"])
        result["negative_predictive_value"] = result["true_negatives"] / (
                result["true_negatives"] + result["false_negatives"])
        result["fn_rate"] = result["false_negatives"] / (result["false_negatives"] + result["true_positives"])
        result["fp_rate"] = result["false_positives"] / (result["false_positives"] + result["true_negatives"])
        return result


