from typing import Union, Callable
from machine_learning_cs7641.assignment_1.experiments.BaseExperiment import BaseExperiment
from machine_learning_cs7641.assignment_1.learners.KNNLearner import KNNLearner


class KNNExperiment(BaseExperiment):

    def initialize_learner(self, n_neighbors: int = 5, weights: Union[str, Callable] = 'uniform',
                           algorithm: str = 'auto',
                           leaf_size: int = 30, p: int = 2, metric: str = 'minkowski', metric_params: dict = None,
                           n_jobs: int = None, log: bool = False):
        self._learner = KNNLearner(n_neighbors, weights, algorithm, leaf_size, p, metric, metric_params, n_jobs, log)

    def run(self, training_set_name: str = None) -> list:
        percentages = [.75, .50, .25]
        n_neighbors = [15, 10, 5, 2]
        leaf_size = [40, 30, 20, 10]
        algorithms = ["ball_tree", "kd_tree"]
        p = [2, 1]

        for percentage in percentages:
            for n_neighbor in n_neighbors:
                for n in leaf_size:
                    for algorithm in algorithms:
                        for power in p:
                            run_name = f"KNN_{n_neighbor}-neighbors_{n}-leaves_{algorithm}_{power}-power"
                            self.initialize_learner(
                                n_neighbors=n_neighbor, leaf_size=n, p=power, algorithm=algorithm, log=self._log
                            )
                            self.experiment_results.append(
                                self.experiment(run_name, percentage, training_set_name=training_set_name)
                            )

        return self.experiment_results
