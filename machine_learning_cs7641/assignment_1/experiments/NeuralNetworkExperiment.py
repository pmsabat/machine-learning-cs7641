from typing import Union

from machine_learning_cs7641.assignment_1.experiments.BaseExperiment import BaseExperiment
from machine_learning_cs7641.assignment_1.learners.NeuralNetworkLearner import NeuralNetworkLearner


class NeuralNetworkExperiment(BaseExperiment):

    def initialize_learner(self, hidden_layer_sizes: tuple = (100,), activation: str = 'relu', solver: str = 'adam',
                           batch_size: Union[int, str] = 'auto', learning_rate: str = 'constant',
                           learning_rate_init: float = .001,
                           power_t: float = .5, tolerance: float = 0.001, max_iter: int = 1, shuffle: bool = False,
                           random_state: int = None, warm_start: bool = False, momentum: float = .9,
                           nesterovs_momentum: bool = True, log: bool = False, early_stopping: bool = False,
                           validation_fraction: float = .1, beta_1: float = .9, beta_2: float = .999,
                           epsilon: float = .00000001,
                           n_iter_no_change: int = 10, max_fun: int = 15000):

        self._learner = NeuralNetworkLearner(
            hidden_layer_sizes=hidden_layer_sizes,
            activation=activation,
            solver=solver,
            batch_size=batch_size,
            learning_rate=learning_rate,
            learning_rate_init=learning_rate_init,
            power_t=power_t,
            tolerance=tolerance,
            max_iter=max_iter,
            shuffle=shuffle,
            random_state=random_state,
            warm_start=warm_start,
            momentum=momentum,
            log=log,
            nesterovs_momentum=nesterovs_momentum,
            early_stopping=early_stopping,
            validation_fraction=validation_fraction,
            beta_1=beta_1,
            beta_2=beta_2,
            epsilon=epsilon,
            n_iter_no_change=n_iter_no_change,
            max_fun=max_fun
        )

    def run(self, training_set_name: str = None) -> list:
        percentages = [.75, .50, .25]
        solver = ["adam", "lbfgs"]
        max_iter = [300, 200, 100, 50]

        for percentage in percentages:
            for n in max_iter:
                for s in solver:
                    run_name = f"NeuralNetwork_{n}-iterations_{s}-solver"
                    self.initialize_learner(max_iter=n, solver=s, log=self._log)
                    self.experiment_results.append(
                        self.experiment(run_name, percentage, training_set_name=training_set_name)
                    )

        return self.experiment_results
