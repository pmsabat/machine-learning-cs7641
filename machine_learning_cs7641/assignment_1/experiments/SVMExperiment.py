from typing import Union

from machine_learning_cs7641.assignment_1.experiments.BaseExperiment import BaseExperiment
from machine_learning_cs7641.assignment_1.learners.SVMLearner import SVMLearner


class SVMExperiment(BaseExperiment):

    def initialize_learner(self, c: float = 1, kernel: str = 'linear', degree: int = 3,
                           gamma: Union[str, float] = 'scale',
                           coefficient0: float = 0.0, shrinking: bool = True, probability: bool = False,
                           tolerance: float = 0.001,
                           cache_size: float = 200, class_weight: Union[dict, str] = None, max_iter: int = 1,
                           decision_function_shape: str = 'ovr', log: bool = False):
        self._learner = SVMLearner(
            c=c,
            kernel=kernel,
            degree=degree,
            gamma=gamma,
            coefficient0=coefficient0,
            shrinking=shrinking,
            probability=probability,
            tolerance=tolerance,
            cache_size=cache_size,
            class_weight=class_weight,
            max_iter=max_iter,
            decision_function_shape=decision_function_shape,
            log=log
        )

    def run(self, training_set_name: str = None) -> list:
        percentages = [.75, .50, .25]
        max_iter = [500, 200, 100, 50, 10]
        kernel = ["linear", "poly", "sigmoid"]
        c_param = [1.0, .8, .6, .4]

        for percentage in percentages:
            for n in max_iter:
                for k in kernel:
                    for c in c_param:
                        run_name = f"SVM_{n}-iterations_{k}-kernel_{c}-C"
                        self.initialize_learner(c=c, max_iter=n, kernel=k, log=self._log)
                        self.experiment_results.append(
                            self.experiment(run_name, percentage, training_set_name=training_set_name)
                        )

        return self.experiment_results
