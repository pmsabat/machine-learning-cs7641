from typing import Union, List
from machine_learning_cs7641.assignment_1.experiments.BaseExperiment import BaseExperiment
from machine_learning_cs7641.assignment_1.learners.DTLearner import DTLearner


class DTExperiment(BaseExperiment):

    def initialize_learner(self, criterion: str = 'gini', splitter: str = 'best', max_depth: int = None,
                           min_samples_split: Union[int, float] = 2, min_samples_leaf: Union[int, float] = 1,
                           min_weight_fraction_leaf: float = 0.0, max_features: Union[int, float, str] = None,
                           random_state: int = None, max_leaf_nodes: int = None, min_impurity_decrease: float = 0.0,
                           class_weight: Union[dict, List[dict], str] = None, ccp_alpha: float = 0.0,
                           log: bool = False):
        self._learner = DTLearner(
            criterion=criterion,
            splitter=splitter,
            max_depth=max_depth,  # affects pruning
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            min_weight_fraction_leaf=min_weight_fraction_leaf,
            max_features=max_features,
            random_state=random_state,
            max_leaf_nodes=max_leaf_nodes,
            min_impurity_decrease=min_impurity_decrease,
            class_weight=class_weight,
            ccp_alpha=ccp_alpha  # affects pruning
        )

    def run(self, training_set_name: str = None) -> list:
        percentages = [.75, .50, .25]
        max_depth = [None, 7, 5, 3]
        algorithms = ["gini", "entropy"]
        ccp_alpha = [.03, .025, .02, .015, 0.0, "manual"]

        for percentage in percentages:
            for n in max_depth:
                for algorithm in algorithms:
                    for cpp in ccp_alpha:
                        run_name = f"DT_{algorithm}_{n}-depth-limit_{cpp}-pruning"
                        self.initialize_learner(
                            criterion=algorithm, max_depth=n, ccp_alpha=cpp if not cpp == "manual" else 0.0,
                            log=self._log
                        )
                        self.experiment_results.append(
                            self.experiment(
                                run_name,
                                percentage,
                                prune=False if not cpp == "manual" else True,
                                training_set_name=training_set_name
                            )
                        )

        return self.experiment_results
