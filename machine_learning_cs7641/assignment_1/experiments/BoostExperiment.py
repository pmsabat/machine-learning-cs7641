from machine_learning_cs7641.assignment_1.experiments.BaseExperiment import BaseExperiment
from machine_learning_cs7641.assignment_1.learners.BoostedLearner import BoostedLearner
from sklearn.tree import DecisionTreeClassifier as DTLearner


class BoostExperiment(BaseExperiment):

    def initialize_learner(self, base_estimator: object = None, n_estimators: int = 50, learning_rate: float = 1.0,
                           algorithm: str = 'SAMME.R', random_state: int = None, log: bool = False):
        self._learner = BoostedLearner(base_estimator, n_estimators, learning_rate, algorithm, random_state, log)

    def run(self, training_set_name: str = None) -> list:
        percentages = [.75, .50, .25]
        estimators = [300, 200, 100]
        algorithms = ["SAMME", "SAMME.R"]
        learning_rate = [1.0, .8, .6]
        learner_types = ["pruning", "normal_dt"]
        learner = {
            "pruning": DTLearner(max_depth=2, ccp_alpha=.05), # much more extreme than other experiments
            "normal_dt": DTLearner(max_depth=2)
        }

        for percentage in percentages:
            for n in estimators:
                for algorithm in algorithms:
                    for lr in learning_rate:
                        for l_type in learner_types:
                            run_name = f"Boosted_{n}-estimators_{algorithm}_{lr}-rate_{l_type}"
                            self.initialize_learner(learner[l_type], n, lr, algorithm, None, self._log)
                            self.experiment_results.append(
                                self.experiment(run_name, percentage, training_set_name=training_set_name)
                            )

        return self.experiment_results
