import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import hiive.mdptoolbox
import hiive.mdptoolbox.mdp
import hiive.mdptoolbox.example
import mdptoolbox
import mdptoolbox.example
import gym
import matplotlib
import matplotlib.pyplot as plt
import time

matplotlib.style.use('ggplot')
smoothing_window = 100
# From https://www.kaggle.com/code/benjaminschreiber/markov-decision-processes/notebook

def compose_discounts(significant_digits):
    prev_discount = 0
    discounts = []
    for i in range(1,significant_digits + 1):
        discounts.append(round(prev_discount + 9*(10**-i),i))
        prev_discount = discounts[-1]
    return discounts

def run_forest(solver, states, discounts, epsilons, probability=0.1, max_iter=10):
    experiments = [] #num states, probability, discount, time, iterations, policy
    for s in states:
        for e in epsilons:
            for d in discounts:
                entry = {}
                P, R = hiive.mdptoolbox.example.forest(S=s, p=probability)
                #start_time = time.time()
                args = {"transitions":P, "reward":R, "gamma":d, "epsilon":e, "max_iter":max_iter, "skip_check":True}
                mdp = solver(args)
                mdp.run()
                #end_time = time.time()
                entry["time"] = mdp.time
                entry["iterations"] = mdp.iter
                entry["policy"] = mdp.policy
                entry["num_states"] = s
                entry["probability"] = probability
                entry["discount"] = d
                entry["epsilon"] = e
                entry["run_stats"] = mdp.run_stats
                experiments.append(entry)
    return experiments

def plot_simple_data(name, x_var, y_var, x_label, y_label, title, figure_size=(4,3)):
    # plt.rcParams["figure.figsize"] = figure_size
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.plot(x_var, y_var, 'o-')
    plt.savefig(f"{name} simple.png")
    plt.close()

def plot_data_legend(name, x_vars, x_label, all_y_vars, y_var_labels, y_label, title, y_bounds=None):
    colors = ['red','orange','black','green','blue','violet']
    plt.rcParams["figure.figsize"] = (4,3)

    i = 0
    for y_var in all_y_vars:
#         if i == 2: # don't plot when i = 1 for cv
#             x_vars = x_vars[1:]
        plt.plot(x_vars, y_var, 'o-', color=colors[i % 6], label=y_var_labels[i])
        i += 1
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    if y_bounds != None:
        plt.ylim(y_bounds)
    leg = plt.legend()
    plt.savefig(f"{name} data_legend.png")
    plt.close()

def make_time_array(run_stats, variables):
    cumulative_sum = 0
    times = []
    output_dict = {v:[] for v in variables}
    output_dict["times"] = times
    for result in run_stats:
        times.append(result["Time"])
        for v in result:
            if v in variables:
                output_dict[v].append(result[v])
    return output_dict


probability_array, reward_matrix = hiive.mdptoolbox.example.forest(S=2000, p=.01)

val_iter_forest = hiive.mdptoolbox.mdp.ValueIteration(probability_array, reward_matrix, 0.999999, epsilon=0.01, max_iter=1000)
val_iter_forest.run()
print(dir(val_iter_forest.run_stats))
fm_val_iter_curated_results = make_time_array(val_iter_forest.run_stats, ["Reward", "Mean V", "Max V", "Iteration"])

plot_simple_data("Val Iter Forest Avg. V", fm_val_iter_curated_results["Iteration"], fm_val_iter_curated_results["Mean V"],
                 "iteration", "Mean Value", "Q-Learning Forest Mgmt Mean Value over Training", figure_size=(6,4))

plot_simple_data("Val Iter Forest Max V", fm_val_iter_curated_results["Iteration"], fm_val_iter_curated_results["Max V"],
                 "iteration", "Max Value", "Q-Learning Forest Mgmt Max Value over Training", figure_size=(6,4))

plot_simple_data("Val Iter Forest Time", fm_val_iter_curated_results["Iteration"], fm_val_iter_curated_results["times"],
                 "iteration", "time elapsed (seconds)", "Q-Learning Forest Mgmt Time Elapsed over Training", figure_size=(6,4))

plot_simple_data("Val Iter Forest Reward", fm_val_iter_curated_results["Iteration"], fm_val_iter_curated_results["Reward"],
                 "iteration", "time elapsed (seconds)", "Q-Learning Forest Mgmt Time Elapsed over Training", figure_size=(6,4))


st = time.time()
fm_q_mdp = hiive.mdptoolbox.mdp.QLearning(probability_array, reward_matrix, 0.999, epsilon=0.1, epsilon_decay=0.95, n_iter=1000000, alpha=0.95, skip_check=True)
fm_q_mdp.run()
print(dir(fm_q_mdp.run_stats[0]))
end = time.time()
q_time_elapsed = end-st
fm_q_curated_results = make_time_array(fm_q_mdp.run_stats, ["Reward", "Mean V", "Max V", "Iteration"])
num_iters = len(fm_q_curated_results["Mean V"])

plot_simple_data("Q Learner Forest Avg. V", fm_q_curated_results["Iteration"], fm_q_curated_results["Mean V"],
                 "iteration", "Mean Value", "Q-Learning Forest Mgmt Mean Value over Training", figure_size=(6,4))

plot_simple_data("Q Learner Forest Max V", fm_q_curated_results["Iteration"], fm_q_curated_results["Max V"],
                 "iteration", "Max Value", "Q-Learning Forest Mgmt Max Value over Training", figure_size=(6,4))

plot_simple_data("Q Learner Forest Time", fm_q_curated_results["Iteration"], fm_q_curated_results["times"],
                 "iteration", "time elapsed (seconds)", "Q-Learning Forest Mgmt Time Elapsed over Training", figure_size=(6,4))

fig2 = plt.figure(figsize=(10,5))
rewards_smoothed = pd.Series(fm_q_curated_results["Reward"],).rolling(smoothing_window, min_periods=smoothing_window).mean()
plt.plot(rewards_smoothed)
plt.xlabel("Iteration")
plt.ylabel("Iteration Reward (Smoothed)")
plt.title("Reward over Iteration (Smoothed over window size {})".format(smoothing_window))
plt.savefig(f"QLearner_forest_episode_reward_over_time.png")
plt.close()
# forest_pi_mdp = mdptoolbox.mdp.PolicyIterationModified(P, R, 0.99999, epsilon=0.01, max_iter=10**6, skip_check=True)

