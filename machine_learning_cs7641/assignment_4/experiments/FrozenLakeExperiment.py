import gym
from gym.envs.toy_text.frozen_lake import FrozenLakeEnv, generate_random_map
from machine_learning_cs7641.assignment_4.Learners.ValueIteration import ValueIteration
from machine_learning_cs7641.assignment_4.Learners.PolicyIteration import PolicyIteration
from machine_learning_cs7641.assignment_4.Learners.QLearner import QLearner

# map = generate_random_map(4)
# print(map)
# print(len(map))
env_exp = gym.make('FrozenLake-v1', desc=None, map_name="4x4", is_slippery=True)
print(env_exp.nS)
print(env_exp.action_space)

value_learner = ValueIteration(env_exp, discount_factor=.9)  # care about all rewards
value_learner.run()
value_learner.plot("FrozenLake-4")

policy_learner = PolicyIteration(env_exp, discount_factor=.9)  # care about all rewards
policy_learner.run()
policy_learner.plot("FrozenLake-4")

q_learner = QLearner(env_exp, random=False, max_episodes=750, discount_factor=.9, log=True)  # care about all rewards
stats = q_learner.run()
print(f"stats were: {stats.episode_rewards}")
q_learner.plot("FrozenLake-4", smoothing_window=10)