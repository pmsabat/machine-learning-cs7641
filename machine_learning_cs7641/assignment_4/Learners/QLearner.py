#!/usr/bin/env python
# From: https://github.com/dennybritz/reinforcement-learning/blob/master/TD/Q-Learning%20Solution.ipynb
from collections import defaultdict
from typing import Optional, Union
from timeit import default_timer as timer
import numpy as np
from machine_learning_cs7641.assignment_4.Learners.BaseLearner import BaseLearner
from machine_learning_cs7641.assignment_4.Learners.Utils.EpisodeStats import EpisodeStats
from machine_learning_cs7641.assignment_4.Learners.Utils.plotting import plot_episode_stats


class QLearner(BaseLearner):

    def __init__(self, env, max_episodes, random=False, epsilon: float = .1, alpha: float = .5,
                 discount_factor: float = 1.0,
                 max_steps: Optional[int] = None, theta: float = 0.00001, log: bool = False):
        super().__init__(env, discount_factor=discount_factor, max_steps=max_steps, theta=theta, log=log)
        self._max_episodes = max_episodes
        self._epsilon = epsilon # change for spaz
        self._alpha = alpha # Learning rate
        self._random = random # random initialization
        self._name = f"QLearner_{'rand' if random else 'greedy'}"
        # Greedy vs Random
        self._q_function = np.random.rand if random else np.zeros
        self._Q = defaultdict(lambda: self._q_function(self._env.action_space.n))
        self.stats = EpisodeStats(max_episodes)
        self._state = None
        self._step_times = []
        self._episode_times = []

    def run(self):
        # Keeps track of useful statistics
        self._policy = self.make_epsilon_greedy_policy()
        for i_episode in range(0, self._max_episodes):
            # Print out which episode we're on, useful for debugging.
            episode_time = 0
            # if (i_episode + 1) % 100 == 0:
            self.log(f"\rEpisode {i_episode + 1}/{self._max_episodes}.")

            self._state = self._env.reset()  # reset env per episode
            # run till done or max_steps reached
            t = 0
            reward = 0
            while self._max_steps is None or t < self._max_steps:
                # take a step
                start_time = timer()
                action, next_state, reward, done = self.step()
                end_time = timer()
                step_time = end_time - start_time
                episode_time += step_time
                self._step_times.append(step_time)
                # Update statistics
                self.stats.episode_rewards[i_episode] += reward
                self.stats.episode_lengths[i_episode] = t

                # TD Update
                best_next_action = np.argmax(self._Q[next_state])
                td_target = reward + self._discount_factor * self._Q[next_state][best_next_action]
                td_delta = td_target - self._Q[self._state][action]
                # self._Q[self._state][action] += self._alpha * td_delta # This version is done via the example

                self._Q[self._state][action] = (self._alpha * td_target) + (1-self._alpha * self._Q[self._state][action]) # This version is the pure version from text
                self._step_delta = max(self._step_delta, abs(td_delta))

                if done:
                    self.log("Done!")
                    break
                t += 1
                self._state = next_state

            self._episode_times.append(episode_time)

            if self._step_delta < self._theta:
                return self.stats
        return self.stats

    def step(self):
        action_probs = self._policy(self._state) # Get the random chance we spazz
        action = np.random.choice(np.arange(len(action_probs)), p=action_probs) # Get our action
        next_state, reward, done, _ = self._env.step(action) # Get the outcome from our env
        if reward > 0:
            self.log(f"\rAction: {action}, Reward: {reward}.")
        return action, next_state, reward, done

    def reset(self):
        super().reset()
        self.stats = EpisodeStats(self._max_episodes)
        self._Q = defaultdict(lambda: self._q_function(self._env.action_space.n))
        self._state = None
        self._episode_times = []

    def make_epsilon_greedy_policy(self):
        """
        Creates an epsilon-greedy policy based on a given Q-function and epsilon.

        Args:
            self: The Q learner Object with state
            self._Q: A dictionary that maps from state -> action-values.
                Each value is a numpy array of length nA (see below)
            self._epsilon: The probability to select a random action. Float between 0 and 1.
            self._env.action_space.n: Number of actions in the environment.

        Returns:
            A function that takes the observation as an argument and returns
            the probabilities for each action in the form of a numpy array of length nA.

        """

        def policy_fn(observation):
            A = np.ones(self._env.action_space.n, dtype=float) * self._epsilon / self._env.action_space.n
            best_action = np.argmax(self._Q[observation])
            A[best_action] += (1.0 - self._epsilon)
            return A

        return policy_fn

    def plot(self, exp_name: str, smoothing_window: int = 10, show=False):
        fig1, fig2, fig3 = plot_episode_stats(self.stats, noshow=(not show), smoothing_window=smoothing_window)
        if not show:
            fig1.savefig(f"QLearner_{exp_name}_episode_length_over_time.png")
            fig2.savefig(f"QLearner_{exp_name}_episode_reward_over_time.png")
            fig3.savefig(f"QLearner_{exp_name}_episode_per_time_step.png")
        return fig1, fig2, fig3

if __name__ == "__main__":
    import matplotlib
    import matplotlib.pyplot as plt
    from gym.envs.toy_text.cliffwalking import CliffWalkingEnv

    matplotlib.style.use('ggplot')

    exp_env = CliffWalkingEnv()
    q_learner = QLearner(exp_env, 500)
    stats = q_learner.run()
    q_learner.plot("unit_test")
