#!/usr/bin/env python
# coding=utf-8
# Based on:
# https://github.com/dennybritz/reinforcement-learning/blob/master/DP/Policy%20Iteration%20Solution.ipynb
from typing import Optional

import numpy as np

from machine_learning_cs7641.assignment_4.Learners.BaseLearner import BaseLearner


class PolicyIteration(BaseLearner):

    def __init__(self, env, discount_factor: float = 1.0,
                 max_steps: Optional[int] = None, theta: float = 0.00001, log: bool = False):
        super().__init__(env=env, discount_factor=discount_factor, max_steps=max_steps, theta=theta, log=log)
        self._name = "Policy_Iteration"

    def reset(self):
        super(PolicyIteration, self).reset()

    def step(self):
        """
        Policy Improvement Algorithm. One iteration of evaluation and improvement of a policy
        To converge, repeat call (aka self.run() ) until an optimal policy is found.

        Returns:
            A tuple (policy, V, reward).
            policy - is the optimal policy, a matrix of shape [S, A] where each state s
            contains a valid probability distribution over actions.
            V -  is the value function for the optimal policy.
            reward - the reward is the reward for following our policy's best actions

        """
        self.log(f"stepping over {self._env.nS}, self-converged is {self.converged()}!")
        delta = 0 # Temp variable to check for change
        # Evaluate the current policy
        self._V = self.policy_eval(self._policy, self._env, self._discount_factor, self._theta)
        self.log(f"V is {self._V}!")

        # Will be set to false if we make any changes to the policy
        policy_stable = True
        reward = 0
        # For each state...
        for s in range(self._env.nS):
            # The best action we would take under the current policy
            chosen_a = np.argmax(self._policy[s])

            # Find the best action by one-step lookahead
            # Ties are resolved arbitarily
            action_values = self.one_step_lookahead(s)
            best_a = np.argmax(action_values)
            best_a_value = np.max(action_values)
            # Calculate delta across all states seen so far
            self._step_delta = max(delta, np.abs(best_a_value - self._V[s]))
            self._step_deltas.append(self._step_delta)
            reward += best_a_value

            # Greedily update the policy
            if chosen_a != best_a:
                policy_stable = False
            self._policy[s] = np.eye(self._env.nA)[best_a]

        # Make sure we've experienced a few things then
        # If the policy is stable we've found an optimal policy. Return it
        if self._steps > 10 and policy_stable:
            self._converged = True
        self.log(f"converged is {self._converged}!")

        return self._policy, self._V, reward


if __name__ == "__main__":
    from gym.envs.toy_text import CliffWalkingEnv
    exp_env = CliffWalkingEnv()
    policy_iterator = PolicyIteration(exp_env, log=True)
    exp_reward = policy_iterator.run()
    policy = policy_iterator.get_policy()
    v = policy_iterator.get_value()
    print("Probability Policy Probability Distribution:")
    print(policy)
    print("")

    print("Reshaped Grid Policy (0=up, 1=right, 2=down, 3=left):")
    print(np.reshape(np.argmax(policy, axis=1), exp_env.shape))
    print("")

    print("Value Function:")
    print(v)
    print("")

    print("Reshaped Grid Value Function:")
    print(v.reshape(exp_env.shape))
    print("")

    expected_v = np.array([0, -1, -2, -3, -1, -2, -3, -2, -2, -3, -2, -1, -3, -2, -1,  0])
    np.testing.assert_array_almost_equal(v, expected_v, decimal=2)

    policy_iterator.plot("unit_test")


