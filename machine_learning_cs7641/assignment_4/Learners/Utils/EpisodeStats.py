import csv

import numpy as np
import pandas

class EpisodeStats(object):
    def __init__(self, num_episodes):
        self.num_episodes = num_episodes
        self.episode_lengths = np.zeros(num_episodes)
        self.episode_times = np.zeros(num_episodes)
        self.episode_rewards = np.zeros(num_episodes)
        self.episode_deltas = np.zeros(num_episodes)

    def to_csv(self, path):
        with open(path, 'w') as f:
            f.write("episode,length,time,reward,delta\n")
            writer = csv.writer(f, delimiter=',')
            writer.writerows(zip(range(self.num_episodes), self.episode_lengths, self.episode_times,
                                 self.episode_rewards, self.episode_deltas))

    @staticmethod
    def from_df(df):
        es = EpisodeStats(df.shape[0])
        es.episode_lengths = df['length'].values
        es.episode_times = df['time'].values
        es.episode_rewards = df['reward'].values
        es.episode_deltas = df['delta'].values

    def from_csv_file(self, path):
        episode_df = pandas.read_csv(path)
        self.episode_lengths = episode_df["length"].values
        self.episode_times = episode_df['time'].values
        self.episode_rewards = episode_df['reward'].values
        self.episode_deltas = episode_df['delta'].values
