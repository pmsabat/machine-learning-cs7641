""""""
"""  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Template for implementing QLearner  (c) 2015 Tucker Balch  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

Template code for CS 4646/7646  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

We do grant permission to share solutions privately with non-students such  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

-----do not edit anything above this line---  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			

Student Name: Paul Sabatino (replace with your name)  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
GT User ID: psabatino3 (replace with your User ID)  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
GT ID: 903474486 (replace with your GT ID)  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
"""

import random as rand
import numpy as np


class QLearner(object):
    """
    This is a Q learner object.

    :param num_states: The number of states to consider.
    :type num_states: int
    :param num_actions: The number of actions available..
    :type num_actions: int
    :param alpha: The learning rate used in the update rule. Should range between 0.0 and 1.0 with 0.2 as a typical value.
    :type alpha: float
    :param gamma: The discount rate used in the update rule. Should range between 0.0 and 1.0 with 0.9 as a typical value.
    :type gamma: float
    :param rar: Random action rate: the probability of selecting a random action at each step. Should range between 0.0 (no random actions) to 1.0 (always random action) with 0.5 as a typical value.
    :type rar: float
    :param radr: Random action decay rate, after each update, rar = rar * radr. Ranges between 0.0 (immediate decay to 0) and 1.0 (no decay). Typically 0.99.
    :type radr: float
    :param dyna: The number of dyna updates for each regular update. When Dyna is used, 200 is a typical value.
    :type dyna: int
    :param verbose: If “verbose” is True, your code can print out information for debugging.
    :type verbose: bool
    """

    def __init__(
            self,
            num_states=100,
            num_actions=4,
            alpha=0.2,
            gamma=0.9,
            rar=0.5,
            radr=0.99,
            dyna=0,
            verbose=False,
    ):
        """
        Constructor method
        """
        self.verbose = verbose
        self.num_actions = num_actions
        self.num_states = num_states
        self.alpha = alpha
        self.gamma = gamma
        self.rar = rar  # Random action rate
        self.radr = radr  # random action decay rate
        self.dyna = dyna
        self.s = 0
        self.a = 0
        self.experiences = set()
        self.q: np.ndarray = np.random.rand(self.num_states, self.num_actions)
        self.t: np.ndarray = np.zeros(
            (self.num_states, self.num_actions, self.num_states))  # need to make it small so we dont have divide by 0
        self.r: np.ndarray = np.zeros((self.num_states, self.num_actions))
        self.r.fill(-1)  # Remember, most rewards are -1.  Walls and quicksand are worse
        self.tc: np.ndarray = np.zeros(
            (self.num_states, self.num_actions, self.num_states))  # need to make it small so we dont have divide by 0
        self.tc.fill(.00001)

    @staticmethod
    def author() -> str:
        return "psabatino3"

    def querysetstate(self, s):
        """
        # update the state based on our pass ins
        Update the state without updating the Q-table

        :param s: The new state
        :type s: int
        :return: The selected action
        :rtype: int
        """
        self.s = s
        # self.experiences = []
        return self.pick_action(s)

    def pick_action(self, state):
        # if self.verbose: print("will we spazz?!")
        chance_to_spazz = np.random.rand()
        action = np.random.randint(0, self.num_actions - 1)
        # if self.verbose: print(f"rar = {self.rar} - chance roll {chance_to_spazz} ")
        if self.rar < chance_to_spazz:
            # if self.verbose: print("didn't spazz!")
            action: int = np.argmax(self.q[state])
        # elif self.verbose and self.rar >= chance_to_spazz: print(f"spazz action = {action}")
        self.a = action
        # if self.verbose: print("done with action!")
        return action

    def tuple_update_q(self, experience: tuple) -> None:
        # if self.verbose: print(f"hallucinating experience = {experience}")
        self.update_q(experience[0], experience[1], experience[2], experience[3])

    def update_q(self, state: int, action: int, state_prime: int, reward: float) -> None:
        # From the lectures - Q'[s, a] = (1 - α) · Q[s, a] + α · (r + γ · Q[s', argmaxa'(Q[s', a'])])
        # if self.verbose: print(f"update_q got s={self.s}, s_prime = {state_prime}, a = {action}, r={reward}")
        self.q[state, action] = (1 - self.alpha) * self.q[self.s, self.a] + self.alpha * (
                    reward + self.gamma * self.q[state_prime, np.argmax(self.q[state_prime, :])])
        # current_q = self.q[self.s,self.a]
        # if self.verbose:
        #     print(f"getting best new state: {self.q[state_prime, :]}")
        # best_prime_state = np.argmax(self.q[state_prime, :])
        # if self.verbose:
        #     print(f"I think the best state is: {best_prime_state}")
        # self.q[state, action] = (
        #     (
        #         (1 - self.alpha) * # The value of past experiences weighted
        #         current_q # The actual reward from the past
        #     ) +
        #     (
        #         self.alpha * # The value of new experiences weighted
        #         (
        #             reward + # The reward we got for this action
        #             self.gamma * # The value of a future reward
        #             self.q[state_prime, best_prime_state]
        #         )
        #     )
        # )

    def query(self, s_prime: int, r: float) -> int:
        """
        Update the Q table and return an action

        :param s_prime: The new state
        :type s_prime: int
        :param r: The immediate reward
        :type r: float
        :return: The selected action
        :rtype: int
        """
        action = self.a
        # if self.verbose: print(f"query got s={self.s}, s_prime = {s_prime}, a = {action}, r={r}")
        # update the state based on our pass ins
        self.q[self.s, action] = (1 - self.alpha) * self.q[self.s, self.a] + self.alpha * (r + self.gamma * self.q[
            s_prime, np.argmax(self.q[s_prime, :])])  # self.update_q(self.s, self.a, s_prime, r)
        if self.dyna > 0:
            self.experiences.add((self.s, self.a, s_prime, r))
            # self.experiences = list(set(self.experiences))
            # if self.verbose: print(f"len experiences = {len(self.experiences)}")
            # if self.verbose: print(f"dyna got s={self.s}, s_prime = {s_prime}, a = {action} - current state:")
            # print(f"dyna was s={self.tc[self.s, self.a, s_prime]}")
            # self.tc[self.s, self.a, s_prime] += 1 # Update to say we saw s_prime after s and a once
            # if self.verbose: print(f"dyna is now s={self.tc[self.s, self.a, s_prime]}")
            # t[s,a,s'] = count(s,a,s')/Sum(count(T[s,a,i]))
            # count(s,a,s') = Tc[s,a,s']
            # sum(count(T[s,a, :]))
            # if self.verbose: print(f"dyna got s={self.s}, s_prime = {s_prime}, a = {action} - current state: {self.tc[self.s, self.a, s_prime]} - possible - {self.tc[self.s,self.a,:]} sum - {self.tc[self.s,self.a,:].sum()}")
            # self.t[self.s, self.a, s_prime] = self.tc[self.s, self.a, s_prime]/np.sum(self.tc[self.s,self.a,:]) # Update probability of s,a -> s'
            # if self.verbose and r is not -1: print(f"reward before: {self.r[self.s,self.a]} - reward recieved: {r} - alpha - {self.alpha}")
            self.r[self.s, self.a] = ((1 - .9) * self.r[self.s, self.a]) + (.9 * r)  # update reward table
            # if self.verbose and r != -1: print(f"reward after: {self.r[self.s,self.a]}")
            experiences: list = rand.choices(list(self.experiences), k=self.dyna)
            # map(lambda experience: print(f"hi, {self.tuple_update_q(experience)}"), experiences) # Doesn't work, don't know why
            [self.update_q(experience[0], experience[1], experience[2], self.r[experience[0], experience[1]]) for
             experience in experiences]
            # self.tuple_update_q(experience)
            # map(lambda x: self.update_q(**x), experiences)
            # for experience in experiences: #for _ in range(self.dyna): # Time to take a trip
            # random_index = np.random.randint(len(self.experiences))
            # experience = self.experiences[random_index]
            # experience = rand.choices(self.experiences, 1)
            # random_state = experience[0] #np.random.randint(0, self.num_states - 1) # random state
            # random_action = experience[1] #np.random.randint(0, self.num_actions - 1) # random action
            # TODO: Figure out why this doesnt work - dyna_state_prime = np.argmax(self.t[random_state, random_action]) # The most likely outcome from the state table T
            # dyna_state_prime = experience[2] #np.argmax(self.t[random_state, random_action]) # The most likely outcome from the state table T
            # dyna_reward = experience[3] # self.r[random_state, random_action]
            # if self.verbose and dyna_reward is not -1: print(f"update_q dyna - s={random_state}, s_prime = {dyna_state_prime}, a = {random_action}, r={dyna_reward}")
            # if self.verbose: print(f"hallucinating experience = {experience}")
            # self.update_q(experience[0], experience[1], experience[2], experience[3])
        # Update state and action
        self.s = s_prime
        action = self.pick_action(self.s)
        self.a = action
        self.rar *= self.radr

        return action


if __name__ == "__main__":
    print("Remember Q from Star Trek? Well, this isn't him")