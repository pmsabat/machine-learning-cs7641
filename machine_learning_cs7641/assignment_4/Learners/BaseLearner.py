from abc import ABC, abstractmethod
import logging
import matplotlib
import numpy as np
import pandas as pd
import pprint
from typing import Optional, Union
from timeit import default_timer as timer

from matplotlib import pyplot as plt
matplotlib.style.use('ggplot')

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)
pp = pprint.PrettyPrinter(indent=2)


class BaseLearner(ABC):

    def __init__(self, env, discount_factor: float = 1.0,
                 max_steps: Optional[int] = None, theta: float = 0.00001, log: bool = False):
        """
        The Base Learner for evaluating different reinforcement learning types, adapted from:
        https://github.com/dennybritz/reinforcement-learning/blob/master/DP/Policy%20Iteration%20Solution.ipynb &
        https://github.com/dennybritz/reinforcement-learning/blob/master/DP/Value%20Iteration%20Solution.ipynb &
        https://github.com/dennybritz/reinforcement-learning/blob/master/TD/Q-Learning%20Solution.ipynb

        Args:
            env: OpenAI env. env.P represents the transition probabilities of the environment.
                env.P[s][a] is a list of transition tuples (prob, next_state, reward, done).
                env.nS is the number of states in the environment.
                env.nA is the number of actions in the environment.
            discount_factor: Gamma discount factor.
            max_steps: The maximum steps to take while running a policy/evaluating a policy
            theta: We stop evaluation once our value function change is less than theta for all states.

        Returns:
            None
        """
        self._env = env
        self._V = np.zeros(env.nS)
        self._policy = np.ones([env.nS, env.nA]) / env.nA
        self._discount_factor = discount_factor
        self._step_delta = 0
        self._delta = 0
        self._theta = theta
        self._steps = 0
        self._step_times = []
        self._rewards_per_step = []
        self._step_deltas = []
        self._converged = False
        self._name = "BaseLearner"
        self._max_steps = max_steps
        self._log = log

    @abstractmethod
    def step(self):
        pass

    def log(self, msg, *args):
        """
        If the learner has verbose set to true, log the message with the given parameters using string.format
        :param msg: The log message
        :param args: The arguments
        :return: None
        """
        if self._log:
            logger.info(pp.pprint(msg.format(*args)))

    def run(self) -> Union[int, float]:
        """
        Policy Improvement Algorithm. Calls step until exhaustion or the optimal policy is found

        Returns:
            reward - the reward is the reward for following our policy's best actions
        """

        # either converge or run until exhaustion
        reward = None
        while not self._converged and (self._max_steps is None or self._steps < self._max_steps):
            start_time = timer()
            _, _, reward = self.step()
            self._rewards_per_step.append(reward)
            end_time = timer()
            step_time = end_time - start_time
            self._step_times.append(step_time)
            self._steps += 1
        return reward

    def converged(self) -> bool:
        """
        Returns:
            Converged -  The bool representation on if the policy converged before max_steps
        """
        return self._converged

    def reset(self):
        self._env = self._env.reset()
        self._V = np.zeros(self._env.nS)
        self._policy = np.ones([self._env.nS, self._env.nA]) / self._env.nA
        self._delta = 0
        self._steps = 0
        self._step_delta = 0
        self._step_times = []
        self._rewards_per_step = []
        self._step_deltas = []
        self._converged = False

    def get_env(self):
        return self._env

    def get_policy(self):
        return self._policy

    def get_value(self):
        return self._V

    def one_step_lookahead(self, state):
        """
        Helper function to calculate the value for all action in a given state.

        Args:
            state: The state to consider (int)
            V: The value to use as an estimator, Vector of length env.nS

        Returns:
            A vector of length env.nA containing the expected value of each action.
        """
        action_function = np.zeros(self._env.nA)
        for a in range(self._env.nA):
            for prob, next_state, reward, done in self._env.P[state][a]:
                action_function[a] += prob * (reward + self._discount_factor * self._V[next_state])
        return action_function

    def get_stats(self):
        return self.get_policy(), self.get_value(), self._step_deltas, \
               self._step_times, self._rewards_per_step, self.converged()

    @staticmethod
    def policy_eval(policy, env, discount_factor=1.0, theta=0.00001):
        """
        Evaluate a policy given an environment and a full description of the environment's dynamics.

        Args:
            policy: [S, A] shaped matrix representing the policy.
            env: OpenAI env. env.P represents the transition probabilities of the environment.
                env.P[s][a] is a list of transition tuples (prob, next_state, reward, done).
                env.nS is a number of states in the environment.
                env.nA is a number of actions in the environment.
            theta: We stop evaluation once our value function change is less than theta for all states.
            discount_factor: Gamma discount factor.

        Returns:
            Vector of length env.nS representing the value function.
        """
        # Start with a random (all 0) value function
        V = np.zeros(env.nS)
        while True:
            delta = 0
            # For each state, perform a "full backup"
            for s in range(env.nS):
                v = 0
                # Look at the possible next actions
                for a, action_prob in enumerate(policy[s]):
                    # For each action, look at the possible next states...
                    for prob, next_state, reward, done in env.P[s][a]:
                        # Calculate the expected value
                        v += action_prob * prob * (reward + discount_factor * V[next_state])
                # How much our value function changed (across any states)
                delta = max(delta, np.abs(v - V[s]))
                V[s] = v
            # Stop evaluating once our value function change is below a threshold
            if delta < theta:
                break
        return np.array(V)

    def plot(self, exp_name: str, smoothing_window: int = 10, show=False):

        # Reward plot
        reward_fig = plt.figure(figsize=(10, 5))
        rewards_smoothed = pd.Series(
            self._rewards_per_step
        ).rolling(
            smoothing_window,
            min_periods=smoothing_window
        ).mean()
        plt.plot(rewards_smoothed)
        plt.xlabel("Iteration (Step #)")
        plt.ylabel("Iteration Reward (Smoothed)")
        plt.title(f"{self._name} - {self._name} Iteration Reward over Iterations (Smoothed over window size {smoothing_window})")
        if not show:
            reward_fig.savefig(f"{self._name}_{exp_name}_reward_over_iterations.png")
        else:
            plt.show()
        plt.close()

        # Delta plot
        delta_fig = plt.figure(figsize=(10, 5))
        # delta_smoothed = pd.Series(
        #     self._step_deltas
        # ).rolling(
        #     smoothing_window,
        #     min_periods=smoothing_window
        # ).mean()
        plt.plot(self._step_deltas)
        plt.xlabel("Iteration (Step #)")
        plt.ylabel("Iteration Delta")
        plt.title(f"{self._name} - {self._name} Iteration Delta over Iterations)")
        if not show:
            delta_fig.savefig(f"{self._name}_{exp_name}_delta_over_iterations.png")
        else:
            plt.show()
        plt.close()

        # Time plot
        time_fig = plt.figure(figsize=(10, 5))
        # times_smoothed = pd.Series(
        #     self._step_times
        # ).rolling(
        #     smoothing_window,
        #     min_periods=smoothing_window
        # ).mean()
        plt.plot(self._step_times)
        plt.xlabel("Iteration (Step #)")
        plt.ylabel("Iteration Reward (Smoothed)")
        plt.title(f"{self._name} - {self._name} Iteration Time over Iterations")
        if not show:
            time_fig.savefig(f"{self._name}_{exp_name}_time_over_iterations.png")
        else:
            plt.show()
        plt.close()

        return reward_fig, delta_fig, time_fig

