#!/usr/bin/env python
# From: https://github.com/dennybritz/reinforcement-learning/blob/master/DP/Value%20Iteration%20Solution.ipynb
from typing import Optional, Union
import numpy as np
from machine_learning_cs7641.assignment_4.Learners.BaseLearner import BaseLearner


class ValueIteration(BaseLearner):

    def __init__(self, env, discount_factor: float = 1.0,
                 max_steps: Optional[int] = None, theta: float = 0.00001, log: bool = False):
        super().__init__(env=env, discount_factor=discount_factor, max_steps=max_steps, theta=theta, log=log)
        self._policy = np.zeros([self._env.nS, self._env.nA])
        self._name = "Value_Iteration"

    def reset(self):
        super().reset()
        self._policy = np.zeros([self._env.nS, self._env.nA])

    def run(self) -> Union[int, float]:
        reward = super().run()

        # we've converged, lets make the policy
        for s in range(self._env.nS):
            # One step lookahead to find the best action for this state
            action = self.one_step_lookahead(s)
            best_action = np.argmax(action)
            # Always take the best action
            self._policy[s, best_action] = 1.0

        return reward

    def step(self):
        # Stopping condition
        delta = 0
        reward = 0
        # Update each state...
        for s in range(self._env.nS):
            # Do a one-step lookahead to find the best action
            action = self.one_step_lookahead(s)
            best_action_value = np.max(action)
            reward += best_action_value
            # Calculate delta across all states seen so far
            delta = max(delta, np.abs(best_action_value - self._V[s]))
            # Update the value function. Ref: Sutton book eq. 4.10.
            self._V[s] = best_action_value
            # Check if we can stop

        self._step_delta = delta
        self._step_deltas.append(self._step_delta)
        if self._step_delta < self._theta:  # we've converged, update state
            self._converged = True

        return self._policy, self._V, reward


if __name__ == "__main__":
    from machine_learning_cs7641.assignment_4.Learners.Utils.gridworld import GridworldEnv
    exp_env = GridworldEnv()
    value_iterator = ValueIteration(exp_env)
    reward = value_iterator.run()
    policy = value_iterator.get_policy()
    v = value_iterator.get_value()
    print("Value Iteration - Policy Probability Distribution:")
    print(policy)
    print("")

    print("Reshaped Grid Policy (0=up, 1=right, 2=down, 3=left):")
    print(np.reshape(np.argmax(policy, axis=1), exp_env.shape))
    print("")

    print("Value Function:")
    print(v)
    print("")

    print("Reshaped Grid Value Function:")
    print(v.reshape(exp_env.shape))
    print("")

    expected_v = np.array([0, -1, -2, -3, -1, -2, -3, -2, -2, -3, -2, -1, -3, -2, -1,  0])
    np.testing.assert_array_almost_equal(v, expected_v, decimal=2)

    value_iterator.plot("unit_test")
