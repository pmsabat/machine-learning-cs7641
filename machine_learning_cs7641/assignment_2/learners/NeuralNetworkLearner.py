from .BaseLearner import BaseLearner
import pandas as pd
import pathlib
from pandas import DataFrame
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import train_test_split
from mlrose.neural import NeuralNetwork
import stackprinter
from typing import Union
stackprinter.set_excepthook(style='darkbg2')


class NeuralNetworkLearner(BaseLearner):

    def __init__(self, hidden_layer_sizes: tuple = (100,), activation: str = 'relu', solver: str = 'adam',
                 batch_size: Union[int, str] = 'auto', learning_rate: str = 'constant', learning_rate_init: float = .001,
                 power_t: float = .5, tolerance: float = 0.001, max_iter: int = 1, shuffle: bool = False,
                 random_state: int = None, warm_start: bool = False, momentum: float = .9,
                 nesterovs_momentum: bool = True, log: bool = False, early_stopping: bool = False,
                 validation_fraction: float = .1, beta_1: float = .9, beta_2: float = .999, epsilon: float = .00000001,
                 n_iter_no_change: int = 10, max_fun: int = 15000) -> None:
        super().__init__(log)
        self.learner = MLPClassifier(
            hidden_layer_sizes=hidden_layer_sizes,
            activation=activation,
            solver=solver,
            batch_size=batch_size,
            learning_rate=learning_rate,
            learning_rate_init=learning_rate_init,
            power_t=power_t,
            tol=tolerance,
            max_iter=max_iter,
            shuffle=shuffle,
            random_state=random_state,
            warm_start=warm_start,
            momentum=momentum,
            verbose=log,
            nesterovs_momentum=nesterovs_momentum,
            early_stopping=early_stopping,
            validation_fraction=validation_fraction,
            beta_1=beta_1,
            beta_2=beta_2,
            epsilon=epsilon,
            n_iter_no_change=n_iter_no_change,
            max_fun=max_fun
        )
        self.__name__ = "NeuralNetworkLearner"

    def plot(self, terminal: bool = True, path: pathlib.Path = None):
        pass


if __name__ == "__main__":
    # This test setup heavily inspired by:
    # https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python
    from sklearn import datasets
    cancer_data_raw: dict = datasets.load_breast_cancer()
    # print("Features: ", cancer_data_raw.feature_names)
    cancer_data: DataFrame = pd.DataFrame(data=cancer_data_raw.data, columns=cancer_data_raw.feature_names)
    # cancer labels (0:malignant, 1:benign)
    cancer_result: DataFrame = pd.DataFrame(data=cancer_data_raw.target, columns=["malign/benign"])

    print(cancer_result.head(5))
    # 70% training and 30% test
    cancer_data_train, cancer_data_test, cancer_result_train, cancer_result_test = train_test_split(cancer_data,
                                                                                                    cancer_result,
                                                                                                    test_size=0.2,
                                                                                                    random_state=109)

    # df_cancer_predictions: DataFrame = pd.DataFame(data=cancer_predictions, columns=["malign/benign"])
    df_cancer_result_test: DataFrame = pd.DataFrame(data=cancer_result_test, columns=["malign/benign"])
    neural_network = NeuralNetworkLearner()
    neural_network.fit(cancer_data_train, cancer_result_train)
    neural_network_accuracy = neural_network.accuracy(cancer_data_test, df_cancer_result_test)

    print(f"NN Accuracy: {neural_network_accuracy}")
