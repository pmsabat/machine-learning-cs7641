from typing import Union

import matplotlib.pyplot as plt
import numpy
import pandas
import seaborn as sns
# work around from:
# https://stackoverflow.com/questions/61867945/python-import-error-cannot-import-name-six-from-sklearn-externals
import six
import sys

sys.modules['sklearn.externals.six'] = six
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.model_selection import train_test_split, learning_curve
from machine_learning_cs7641.assignment_2.experiments.BaseExperiment import BaseOptimizerExperiment
from timeit import default_timer as timer
from sklearn.neural_network import MLPClassifier

from mlrose import GeomDecay
from mlrose.neural import NeuralNetwork
import warnings
warnings.filterwarnings("ignore")

class NeuralNetworkExperiment(BaseOptimizerExperiment):

    def __init__(self, problem, max_attempts, max_iters, init_state, curve=True, log: bool = False):
        super(NeuralNetworkExperiment, self).__init__(problem, max_attempts, max_iters, init_state, curve=curve,
                                                      log=log)

        self._learner = MLPClassifier(
            solver="adam",
            activation='relu',
            max_iter=300,
            learning_rate_init=.001
        )

        self.temperature = GeomDecay(init_temp=100.0, decay=0.95, min_temp=0.00001)
        self._sa_ann = NeuralNetwork(
            activation='relu', algorithm='simulated_annealing', is_classifier=True, early_stopping=True, random_state=1,
            max_attempts=200, max_iters=300, curve=True, schedule=self.temperature
        )

        self._ga_ann = NeuralNetwork(
            activation='relu', algorithm='genetic_alg', is_classifier=True, early_stopping=True, random_state=1,
            max_attempts=200, max_iters=300, curve=True
        )

        self._rhc_ann = NeuralNetwork(
            activation='relu', algorithm='random_hill_climb', is_classifier=True, early_stopping=True, random_state=1,
            max_attempts=200, max_iters=300, curve=True
        )
        self._name="NeuralNetworks_Experiment"
        self._percentage = .25
        self._x_data_train = None
        self._x_data_test = None
        self._y_data_train = None
        self._y_data_test = None
        self._data = None
        self._results = None
        self._nn_cm = None
        self._sa_cm = None
        self._ga_cm = None
        self._rhc_cm = None
        self._nn_train_sizes = None
        self._sa_train_sizes = None
        self._ga_train_sizes = None
        self._rhc_train_sizes = None
        self._nn_train_scores = None
        self._sa_train_scores = None
        self._ga_train_scores = None
        self._rhc_train_scores = None
        self._nn_fit_times = None
        self._sa_fit_times = None
        self._ga_fit_times = None
        self._rhc_fit_times = None
        self._nn_fit_scores = None
        self._sa_fit_scores = None
        self._ga_fit_scores = None
        self._rhc_fit_scores = None
        self._nn_score_times = None
        self._sa_score_times = None
        self._ga_score_times = None
        self._rhc_score_times = None

    def plot_learning_curve(
            self, title, train_scores, test_scores, fit_times, score_times,
            ylim=None, axes=None, train_sizes=numpy.linspace(0.1, 1.0, 5)
    ):
        """
            Generate 3 plots: the test and training learning curve, the training
            samples vs fit times curve, the fit times vs score curve.

            Parameters
            ----------

            title : str
                Title for the chart.

            axes : array-like of shape (3,), default=None
                Axes to use for plotting the curves.

            ylim : tuple of shape (2,), default=None
                Defines minimum and maximum y-values plotted, e.g. (ymin, ymax).

            train_sizes : array-like of shape (n_ticks,)
                Relative or absolute numbers of training examples that will be used to
                generate the learning curve. If the ``dtype`` is float, it is regarded
                as a fraction of the maximum size of the training set (that is
                determined by the selected validation method), i.e. it has to be within
                (0, 1]. Otherwise it is interpreted as absolute sizes of the training
                sets. Note that for classification the number of samples usually have
                to be big enough to contain at least one sample from each class.
                (default: np.linspace(0.1, 1.0, 5))
        """
        plt.figure()
        if axes is None:
            _, axes = plt.subplots(1, 3, figsize=(20, 5))

        axes[0].set_title(title)
        if ylim is not None:
            axes[0].set_ylim(*ylim)
        axes[0].set_xlabel("Training examples")
        axes[0].set_ylabel("Score")

        train_scores_mean = numpy.mean(train_scores, axis=1)
        train_scores_std = numpy.std(train_scores, axis=1)
        test_scores_mean = numpy.mean(test_scores, axis=1)
        test_scores_std = numpy.std(test_scores, axis=1)
        fit_times_mean = numpy.mean(fit_times, axis=1)
        fit_times_std = numpy.std(fit_times, axis=1)

        # Plot learning curve
        axes[0].grid()
        axes[0].fill_between(
            train_sizes,
            train_scores_mean - train_scores_std,
            train_scores_mean + train_scores_std,
            alpha=0.1,
            color="r",
        )
        axes[0].fill_between(
            train_sizes,
            test_scores_mean - test_scores_std,
            test_scores_mean + test_scores_std,
            alpha=0.1,
            color="g",
        )
        axes[0].plot(
            train_sizes, train_scores_mean, "o-", color="r", label="Training score"
        )
        axes[0].plot(
            train_sizes, test_scores_mean, "o-", color="g", label="Cross-validation score"
        )
        axes[0].legend(loc="best")

        # Plot n_samples vs fit_times
        axes[1].grid()
        axes[1].plot(train_sizes, fit_times_mean, "o-")
        axes[1].fill_between(
            train_sizes,
            fit_times_mean - fit_times_std,
            fit_times_mean + fit_times_std,
            alpha=0.1,
        )
        axes[1].set_xlabel("Training examples")
        axes[1].set_ylabel("fit_times")
        axes[1].set_title("Scalability of the model")

        # Plot fit_time vs score
        fit_time_argsort = fit_times_mean.argsort()
        fit_time_sorted = fit_times_mean[fit_time_argsort]
        test_scores_mean_sorted = test_scores_mean[fit_time_argsort]
        test_scores_std_sorted = test_scores_std[fit_time_argsort]
        axes[2].grid()
        axes[2].plot(fit_time_sorted, test_scores_mean_sorted, "o-")
        axes[2].fill_between(
            fit_time_sorted,
            test_scores_mean_sorted - test_scores_std_sorted,
            test_scores_mean_sorted + test_scores_std_sorted,
            alpha=0.1,
        )
        axes[2].set_xlabel("fit_times")
        axes[2].set_ylabel("Score")
        axes[2].set_title("Performance of the model")

        plt.savefig(f"./figures/{self._name}_{title}.png")
        plt.close()

    def plot_confusion_matrix(self, cm, name, colors="Blues"):
        # From - https://www.stackvidhya.com/plot-confusion-matrix-in-python-and-why/
        plt.figure()
        ax = sns.heatmap(cm, annot=True, cmap=colors)

        ax.set_title(f'Confusion Matrix for {name} with labels');
        ax.set_xlabel('\nPredicted Values')
        ax.set_ylabel('Actual Values ');

        ## Ticket labels - List must be in alphabetical order
        ax.xaxis.set_ticklabels(['False', 'True'])
        ax.yaxis.set_ticklabels(['False', 'True'])

        ## Save the visualization of the Confusion Matrix.
        plt.savefig(f'./figures/{self._name}_{name}_cm.png')
        plt.close()

    def plot(self) -> None:
        plt.close()
        self.plot_confusion_matrix(self._nn_cm, "NN_Best")
        self.plot_confusion_matrix(self._sa_cm, "SA_NN")
        self.plot_confusion_matrix(self._ga_cm, "GA_NN")
        self.plot_confusion_matrix(self._rhc_cm, "RHC_NN")
        self.plot_learning_curve(
            "Best_NN", self._nn_train_scores, self._nn_fit_scores, self._nn_fit_times, self._nn_score_times
        )
        self.plot_learning_curve(
            "SA_NN", self._sa_train_scores, self._sa_fit_scores, self._sa_fit_times, self._sa_score_times
        )
        self.plot_learning_curve(
            "GA_NN", self._ga_train_scores, self._ga_fit_scores, self._ga_fit_times, self._ga_score_times
        )
        self.plot_learning_curve(
            "RHC_NN", self._rhc_train_scores, self._rhc_fit_scores, self._rhc_fit_times, self._rhc_score_times
        )
        plt.close()

    def generate_problem(self, seed=None):
        phishing_data = pandas.read_csv("../assignment_1/data/spam.csv")
        self._results = phishing_data["class"].to_frame()
        self._data = phishing_data.drop(columns=["class"])

    def run_experiment(self) -> dict:
        if self._data is None:
            try:
                self.generate_problem()
            except Exception as e:
                print(f"You must install all dependencies including data and run this from the correct directory!")
                raise e

        result = {
            "run_name": "neural_network_learner_optimized",
            "samples": (self._percentage * self._data.size),
            "dataset": "SPAM"
        }

        print(f"The number of unique y is: {numpy.unique(self._results)}")
        self._x_data_train, self._x_data_test, self._y_data_train, self._y_data_test = train_test_split(
            self._data,
            self._results,
            test_size=1.0-self._percentage,
            random_state=0  # make it repeatable
        )

        """ 
            Now we have are running on the original best results for phishing!
            This is not an "optimized" learner problem, but our best baseline        
        """

        nn_fit_time = timer()
        self._learner.fit(self._x_data_train, self._y_data_train.values.ravel())
        nn_fit_time_end = timer()
        result["nn_fit_time"] = nn_fit_time_end - nn_fit_time

        experiment_1_query_time = timer()
        predict_results = self._learner.predict(self._x_data_test)
        experiment_1_query_time_end = timer()
        result["nn_query_time"] = experiment_1_query_time_end - experiment_1_query_time

        #  https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal
        result["nn_accuracy"] = accuracy_score(predict_results, self._y_data_test.values.ravel())
        self._nn_cm = confusion_matrix(predict_results, self._y_data_test.values.ravel())

        # Reset the learner for next phase
        self._learner = MLPClassifier(
            solver="adam",
            activation='relu',
            max_iter=300,
            learning_rate_init=.001
        )
        self._nn_train_sizes, self._nn_train_scores, self._nn_fit_scores, \
            self._nn_fit_times, self._nn_score_times = learning_curve(
                self._learner,
                self._data,
                self._results,
                cv=4,
                return_times=True,
            )

        """ 
            Now we have are running on the SA for best settings from phishing!
            This is an "optimized" (SA) learner problem, vs our best baseline        
        """
        sa_fit_time = timer()
        self._sa_ann.fit(self._x_data_train, self._y_data_train.values.ravel())
        sa_fit_time_end = timer()
        result["sa_fit_time"] = sa_fit_time_end - sa_fit_time

        sa_query_time = timer()
        predict_results = self._sa_ann.predict(self._x_data_test)
        sa_query_time_end = timer()
        result["sa_query_time"] = sa_query_time_end - sa_query_time

        # #  https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal
        result["sa_accuracy"] = accuracy_score(predict_results, self._y_data_test.values.ravel())
        print(f"The SA predictions were: {numpy.unique(predict_results)}")
        self._sa_cm = confusion_matrix(predict_results, self._y_data_test.values.ravel())
        #
        # # Reset the learner for next phase
        self._sa_ann = NeuralNetwork(
            activation='relu', algorithm='simulated_annealing', is_classifier=True, random_state=1,
            max_attempts=200, max_iters=300, curve=True, schedule=self.temperature
        )
        self._sa_train_sizes, self._sa_train_scores, self._sa_fit_scores, \
            self._sa_fit_times, self._sa_score_times = learning_curve(
                self._sa_ann,
                self._data,
                self._results,
                cv=4,
                return_times=True,
            )

        print(f"At the end the accuracy was: {result['sa_accuracy']}")
        print(f"At the end the confusion matrix was: {self._sa_cm}")

        """ 
            Now we have are running on the GA for best settings from phishing!
            This is an "optimized" (GA) learner problem, vs our best baseline        
        """
        ga_fit_time = timer()
        self._ga_ann.fit(self._x_data_train, self._y_data_train.values.ravel())
        ga_fit_time_end = timer()
        result["ga_fit_time"] = ga_fit_time_end - ga_fit_time

        ga_query_time = timer()
        predict_results = self._ga_ann.predict(self._x_data_test)
        ga_query_time_end = timer()
        result["ga_query_time"] = ga_query_time_end - ga_query_time
        print(f"The GA predictions were: {numpy.unique(predict_results)}")

        #  https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-negative-false-positive-and-fal
        result["ga_accuracy"] = accuracy_score(predict_results, self._y_data_test.values.ravel())
        self._ga_cm = confusion_matrix(predict_results, self._y_data_test.values.ravel())



        print(f"At the end the ga accuracy was: {result['ga_accuracy']}")
        print(f"At the end the ga confusion matrix was: {self._ga_cm}")

        # Reset the learner for the next phase
        self._ga_ann = NeuralNetwork(
            activation='relu', algorithm='genetic_alg', is_classifier=True, early_stopping=True, random_state=1,
            max_attempts=200, curve=True
        )
        self._ga_train_sizes, self._ga_train_scores, self._ga_fit_scores, \
            self._ga_fit_times, self._ga_score_times = learning_curve(
                self._ga_ann,
                self._data,
                self._results,
                cv=4,
                return_times=True,
            )

        # """
        #     Now we have are running on the RHC for best settings from phishing!
        #     This is an "optimized" (RHC) learner problem, vs our best baseline
        # """
        rhc_fit_time = timer()
        self._rhc_ann.fit(self._x_data_train, self._y_data_train.values.ravel())
        rhc_fit_time_end = timer()
        result["rhc_fit_time"] = rhc_fit_time_end - rhc_fit_time

        rhc_query_time = timer()
        predict_results = self._rhc_ann.predict(self._x_data_test)
        rhc_query_time_end = timer()
        result["rhc_query_time"] = rhc_query_time_end - rhc_query_time
        print(f"The RHC predictions were: {numpy.unique(predict_results)}")

        #  https://stackoverflow.com/questions/31324218/scikit-learn-how-to-obtain-true-positive-true-nerhctive-false-positive-and-fal
        result["rhc_accuracy"] = accuracy_score(predict_results, self._y_data_test)
        self._rhc_cm = confusion_matrix(predict_results, self._y_data_test)


        print(f"At the end the rhc accuracy was: {result['rhc_accuracy']}")
        print(f"At the end the rhc confusion matrix was: {self._rhc_cm}")

        # Reset the learner for the next phase
        self._rhc_ann = NeuralNetwork(
            activation='relu', algorithm='random_hill_climb', is_classifier=True, early_stopping=True, random_state=1,
            max_attempts=200, curve=True
        )
        self._rhc_train_sizes, self._rhc_train_scores, self._rhc_fit_scores, \
            self._rhc_fit_times, self._rhc_score_times = learning_curve(
                self._rhc_ann,
                self._data,
                self._results,
                cv=4,
                return_times=True,
            )
        return result
