from machine_learning_cs7641.assignment_2.experiments.BaseExperiment import BaseOptimizerExperiment
# work around from:
# https://stackoverflow.com/questions/61867945/python-import-error-cannot-import-name-six-from-sklearn-externals
import six
import sys
sys.modules['sklearn.externals.six'] = six
from mlrose import Knapsack, DiscreteOpt
import numpy as np


class KnapSackExperiment(BaseOptimizerExperiment):

    def __init__(self, problem, max_attempts, max_iters, init_state, curve=True, log: bool = False):
        super(KnapSackExperiment, self).__init__(problem, max_attempts, max_iters, init_state, curve=curve, log=log)
        self.name = "Knap_Sack_Experiment"

    def generate_problem(self, seed=None):
        # NP Hard problem - TSP
        # Lets make our fitness function, 100 random coordinates from 1 to 1000
        # generate 20 random ints one time for weight, then use 20 values 1 to 20 for values
        #
        if seed:
            np.random.seed(seed)
        weights = [np.random.randint(1, 20) for i in range(0,20)]
        values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        ks_fitness = Knapsack(weights, values, max_weight_pct=.5)
        self._problem = DiscreteOpt(length=len(values), fitness_fn=ks_fitness, maximize=True)
