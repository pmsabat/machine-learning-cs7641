from abc import ABC, abstractmethod
import logging as logger
# work around from:
# https://stackoverflow.com/questions/61867945/python-import-error-cannot-import-name-six-from-sklearn-externals
import six
import sys
sys.modules['sklearn.externals.six'] = six
from mlrose import mimic, simulated_annealing, random_hill_climb, genetic_alg, ExpDecay
from matplotlib import pyplot as plt
import seaborn as sns
from timeit import default_timer as timer

class BaseOptimizerExperiment(ABC):

    def __init__(self, problem, max_attempts, max_iters, init_state, curve=True, log: bool = False):
        self.name = "BaseExperiment"
        self._problem = problem
        self.max_attempts = max_attempts
        self.max_iters = max_iters
        self.init_state = init_state
        self.curve = curve
        self._log: bool = log
        self.mimic_best_state = None
        self.mimic_best_fitness = None
        self.mimic_fitness_curve = None
        self.rhc_best_state = None
        self.rhc_best_fitness = None
        self.rhc_fitness_curve = None
        self.sa_best_state = None
        self.sa_best_fitness = None
        self.sa_fitness_curve = None
        self.ga_best_state = None
        self.ga_best_fitness = None
        self.ga_fitness_curve = None
        self.ga_time = None
        self.sa_time = None
        self.mimic_time = None
        self.rhc_time = None
        self.log = log

    def answer_to_dict(self) -> dict:
        return {
            "mimic_best_state": self.mimic_best_state,
            "mimic_best_fitness": self.mimic_best_fitness,
            "mimic_fitness_curve": self.mimic_fitness_curve,
            "rhc_best_state": self.rhc_best_state,
            "rhc_best_fitness": self.rhc_best_fitness,
            "rhc_fitness_curve": self.rhc_fitness_curve,
            "ga_best_state": self.ga_best_state,
            "ga_best_fitness": self.ga_best_fitness,
            "ga_fitness_curve": self.ga_fitness_curve,
            "sa_best_state": self.sa_best_state,
            "sa_best_fitness": self.sa_best_fitness,
            "sa_fitness_curve": self.sa_fitness_curve,
        }

    def log(self, msg: str):
        if self._log:
            logger.info(msg)

    @abstractmethod
    def generate_problem(self, seed=None):
        pass

    def plot(self) -> None:
        plt.figure()
        sns.lineplot(data=self.sa_fitness_curve, label='SA')
        sns.lineplot(data=self.ga_fitness_curve, label='GA')
        sns.lineplot(data=self.rhc_fitness_curve, label='RHC')
        sns.lineplot(data=self.mimic_fitness_curve, label='MIMIC')
        plt.legend()
        plt.ylabel('Fitness Score')
        plt.xlabel('Number of Iterations')
        plt.title(f'Fitness vs. Number of Iterations {self.name}')
        plt.savefig(f'./figures/{self.name}_fitness_curve.png')
        plt.close()

        print(f"Time to run {self.name} ga was: {self.ga_time}")
        print(f"Time to run {self.name} sa was: {self.sa_time}")
        print(f"Time to run {self.name} rhc was: {self.rhc_time}")
        print(f"Time to run {self.name} mimic was: {self.mimic_time}")

    def run_experiment(self) -> dict:
        if self._problem is None:
            self.generate_problem()  # use reasonable defaults if experiment is not initiated

        ga_start = timer()
        self.ga_best_state, self.ga_best_fitness, self.ga_fitness_curve = genetic_alg(
            self._problem,
            max_attempts=self.max_attempts,
            max_iters=self.max_iters,
            curve=self.curve
        )
        ga_stop = timer()
        self.ga_time = ga_stop - ga_start
        print("Done genetic")

        # simulated annealing
        temperature = ExpDecay()  # init_temp=1.0, exp_const=0.005, min_temp=0.001)
        sa_start = timer()
        self.sa_best_state, self.sa_best_fitness, self.sa_fitness_curve = simulated_annealing(
            self._problem,
            schedule=temperature,
            max_attempts=self.max_attempts,
            max_iters=self.max_iters,
            curve=self.curve
        )
        sa_stop = timer()
        self.sa_time = sa_stop - sa_start

        print("Done SA")

        # hill climb
        rhc_start = timer()
        self.rhc_best_state, self.rhc_best_fitness, self.rhc_fitness_curve = random_hill_climb(
            self._problem,
            max_attempts=self.max_attempts,
            max_iters=self.max_iters,
            curve=self.curve
        )
        rhc_stop = timer()
        self.rhc_time = rhc_stop - rhc_start

        print("Done rhc")

        # mimic
        mimic_start = timer()
        self.mimic_best_state, self.mimic_best_fitness, self.mimic_fitness_curve = mimic(
            self._problem,
            max_attempts=self.max_attempts,
            max_iters=self.max_iters,
            curve=self.curve
        )
        mimic_stop = timer()
        self.mimic_time = mimic_stop - mimic_start

        print("Done mimic")

        return self.answer_to_dict()
