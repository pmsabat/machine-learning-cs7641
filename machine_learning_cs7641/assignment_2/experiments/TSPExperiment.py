import pandas
import numpy as np
from machine_learning_cs7641.assignment_2.experiments.BaseExperiment import BaseOptimizerExperiment
from mlrose import TravellingSales, TSPOpt


class TSPExperiment(BaseOptimizerExperiment):
    
    def __init__(self, problem, max_attempts, max_iters, init_state, curve=True, log: bool = False):
        super(TSPExperiment, self).__init__(problem, max_attempts, max_iters, init_state, curve=curve, log=log)
        self.name = "TSP_Experiment"

    def generate_problem(self, seed=None):
        # NP Hard problem - TSP
        # Lets make our fitness function, 100 random coordinates from 1 to 1000
        tsp_size = 30
        if seed:
            np.random.seed(seed)
        traveling_points = pandas.DataFrame(
            [(np.random.randint(1, 500), np.random.randint(1, 500)) for i in range(0, tsp_size)],
            columns=["x", "y"]
        )
        tsp_fitness = TravellingSales(coords=traveling_points.to_numpy())
        self._problem = TSPOpt(length=tsp_size, fitness_fn=tsp_fitness, maximize=False)
