from machine_learning_cs7641.assignment_2.experiments.BaseExperiment import BaseOptimizerExperiment
# work around from:
# https://stackoverflow.com/questions/61867945/python-import-error-cannot-import-name-six-from-sklearn-externals
import six
import sys
sys.modules['sklearn.externals.six'] = six
from mlrose import FourPeaks, DiscreteOpt


class FourPeaksExperiment(BaseOptimizerExperiment):

    def __init__(self, problem, max_attempts, max_iters, init_state, curve=True, log: bool = False):
        super(FourPeaksExperiment, self).__init__(problem, max_attempts, max_iters, init_state, curve=curve, log=log)
        self.name = "Four_Peaks_Experiment"

    def generate_problem(self, seed=None):
        # NP Hard problem - TSP
        # Lets make our fitness function, 100 random coordinates from 1 to 1000
        fitness = FourPeaks()
        size = 500
        self._problem = DiscreteOpt(size, fitness, maximize=True)

