# work around from:
# https://stackoverflow.com/questions/61867945/python-import-error-cannot-import-name-six-from-sklearn-externals
import six
import sys
sys.modules['sklearn.externals.six'] = six

from machine_learning_cs7641.assignment_2.experiments.TSPExperiment import TSPExperiment
from machine_learning_cs7641.assignment_2.experiments.FourPeaksExperiment import FourPeaksExperiment
from machine_learning_cs7641.assignment_2.experiments.KnapSackExperiment import KnapSackExperiment

static_state = 99959
#
tsp_experiment = TSPExperiment(None, 300, 500, 99959)
tsp_experiment.generate_problem(static_state)
tsp_experiment.run_experiment()
tsp_experiment.plot()
#
# print(f"the result of TSP was: {tsp_experiment.answer_to_dict()}")
#
peaks_4_experiment = FourPeaksExperiment(None, 300, 500, 99959)
peaks_4_experiment.generate_problem(static_state)
peaks_4_experiment.run_experiment()
peaks_4_experiment.plot()

# print(f"the result of four peaks was: {peaks_4_experiment.answer_to_dict()}")
#
ks_experiment = KnapSackExperiment(None, 300, 100, 99959)
ks_experiment.generate_problem(static_state)
ks_experiment.run_experiment()
ks_experiment.plot()
#
# print(f"the result of knap sacks was: {ks_experiment.answer_to_dict()}")

# nn_experiment = NeuralNetworkExperiment(None, 200, 500, static_state)
# nn_experiment.run_experiment()
# nn_experiment.plot()
