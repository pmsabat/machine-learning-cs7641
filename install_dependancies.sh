#!/bin/bash

# install base stuff
sudo apt install curl git python3 python3-distutils python3-apt -y

# install asdf incase you have an older VM with older python installed
git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.9.0
echo ". $HOME/.asdf/asdf.sh" >> ~/.bashrc #adds bashrc to your user profile
echo ". $HOME/.asdf/completions/asdf.bash" >> ~/.bashrc # adds auto-completes

source ~/.bashrc

asdf plugin-add python # get asdf setup to handle python
asdf install python 3.10 # add the version for this project
asdf global python system # preserve the normal system python

# install poetry for package management
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 - # install poetry

# Get data
curl -o ./machine_learning_cs7641/assignment_1/data/penguins_size.csv https://www.kaggle.com/parulpandey/palmer-archipelago-antarctica-penguin-data?select=penguins_size.csv
curl -o ./machine_learning_cs7641/assignment_1/data/phishing.csv https://www.openml.org/data/get_csv/1798106/phpV5QYya
curl -o ./machine_learning_cs7641/assignment_1/data/spam.csv https://www.openml.org/data/get_csv/44/dataset_44_spambase.arff
